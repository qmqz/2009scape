package rs09.game.content.dialogue.region.burthorpe

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.UNFERTH_2655
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class UnferthDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        //     if (isQuestComplete(player, "A Tail of Two Cats")) {
        //    p(ASKING, "Hi Unferth. How's Bob?").also { stage = 10 }
        //    } else {
            n(HALF_WORRIED, "Sorry, no time to talk.").also { stage = END_DIALOGUE }
        //   }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            10 -> nl(SAD, "I don't see him as much as I used to but he's fine.").also { stage++ }
            11 -> p(HALF_WORRIED, "Feeling OK?").also { stage++ }
            12 -> n(SUSPICIOUS, "Well my spleen is playing up again...").also { stage++ }
            13 -> p(WORRIED, "I must go, something came up!").also { stage++ }
            14 -> n(FRIENDLY, "Oh, OK.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(UNFERTH_2655)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return UnferthDialogue(player)
    }
}