package rs09.game.content.dialogue.region.alkharid

import api.addItemOrDrop
import api.amountInInventory
import api.removeItem
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable
import org.rs09.consts.Items.COINS_995
import org.rs09.consts.Items.KEBAB_1971
import org.rs09.consts.NPCs.KARIM_543
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class KarimDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(ASKING, "Would you like to buy a nice kebab? Only one gold.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("I think I'll give it a miss.", "Yes please.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(NEUTRAL, "I think I'll give it a miss.").also { stage = END_DIALOGUE }
                2 -> player(FRIENDLY, "Yes please.").also { stage = 10 }
            }

            10 ->{
                if (amountInInventory(player, COINS_995) >= 1) {
                    removeItem(player, Item(COINS_995, 1))
                    addItemOrDrop(player, KEBAB_1971)
                    end()
                } else {
                    player(WORRIED, "Oops, I forgot to bring any money with me.").also { stage++ }
                }
            }
            11 -> npc(ANNOYED, "Come back when you have some.").also { stage = END_DIALOGUE }


        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(KARIM_543)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return KarimDialogue(player)
    }
}