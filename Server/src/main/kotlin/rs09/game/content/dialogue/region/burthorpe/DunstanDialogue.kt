package rs09.game.content.dialogue.region.burthorpe

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.DUNSTAN_1082
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class DunstanDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY, "Hi!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(FRIENDLY, "Hi! Did you want something?").also { stage++ }
            1 -> options("Is it okay if I use your anvil?", "Can you repair my items for me?", "Nothing, thanks.").also { stage++ }
            2 -> when (buttonId) {
                1 -> npc(ASKING, "So you're a smith are you?").also { stage = 10 }
                2 -> npcl(FRIENDLY, "Of course I can, though the materials may cost you. Just hand me what you've got and I'll have a look.").also { stage = END_DIALOGUE }
                3 -> npc(NEUTRAL, "All right. Speak to you later then.").also { stage = END_DIALOGUE }
            }

            10 -> player(HALF_GUILTY, "I dabble.").also { stage++ }
            11 -> npcl(FRIENDLY, "A fellow smith is welcome to use my anvil!").also { stage++ }
            12 -> player(FRIENDLY, "Thanks!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(DUNSTAN_1082)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return DunstanDialogue(player)
    }
}