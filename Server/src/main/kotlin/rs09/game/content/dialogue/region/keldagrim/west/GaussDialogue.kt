package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.GAUSS_2196
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class GaussDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(OLD_DEFAULT,"I say, I say, a toast to this fine human!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> player(ASKING, "What? I mean, err... why?").also { stage++ }
            1 -> npc(OLD_DEFAULT, "Why not!").also { stage++ }
            2 -> playerl(GUILTY, "I don't have anything suitable to toast with.").also { stage++ }
            3 -> npc(OLD_DEFAULT, "Then go get something from the bar!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return GaussDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(GAUSS_2196)
    }
}