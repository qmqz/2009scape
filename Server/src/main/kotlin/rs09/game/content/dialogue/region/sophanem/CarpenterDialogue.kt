package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.CARPENTER_1981
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class CarpenterDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hey there. How're you holding up?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(FRIENDLY, "Not too bad. Still, I hope we can get rid of these spots soon.").also { stage++ }
            1 -> npcl(FRIENDLY, "The embalmer has been importing a soothing balm from a merchant called Ali M. It's meant to stop the itching.").also { stage++ }
            2 -> player(ASKING, "Does it work?").also { stage++ }
            3 -> npcl(NEUTRAL, "Not even remotely, but it sure does keep the flies away.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(CARPENTER_1981)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return CarpenterDialogue(player)
    }
}