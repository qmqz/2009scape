package rs09.game.content.dialogue.region.keldagrim.palace.marketplace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.GULLDAMAR_2159
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class GulldamarDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(OLD_CALM_TALK2,"Finest silver in all of Keldagrim, come and see!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> o("Right, what do you have?", "Not interested, thanks.").also { stage++ }

            1 -> when(buttonId) {
                1 -> p(FRIENDLY, "Right, what do you have?").also { stage = 9 }
                2 -> p(FRIENDLY, "No thanks.").also { stage = END_DIALOGUE }
            }
            9 -> n(OLD_LAUGH1, "Silver, what else!").also { stage++ }
            10 -> npc.openShop(player)
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return GulldamarDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(GULLDAMAR_2159)
    }
}