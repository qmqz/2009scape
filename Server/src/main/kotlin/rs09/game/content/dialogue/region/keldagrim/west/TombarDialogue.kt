package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.TOMBAR_2199
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class TombarDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(ASKING, "Say, aren't you a bit tall for a dwarf?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(OLD_BOWS_HEAD_SAD, "Was there anything in particular you wanted?").also { stage++ }
            1 -> options("I'd like a quest please.", "No, I just like talking to strangers.").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(FRIENDLY, "I'd like a quest please.").also { stage = 10 }
                2 -> player(NEUTRAL, "No, I just like talking to strangers.").also { stage = 20 }
            }

            10 -> npcl(OLD_CALM_TALK1, "I have nothing to do for you, I'm afraid. Ask around town, though, there are always people who need some work done around here.").also { stage = END_DIALOGUE }

            20 -> npc(OLD_DISTRESSED, "Well I don't.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(TOMBAR_2199)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return TombarDialogue(player)
    }
}