package rs09.game.content.dialogue.region.canifis

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.TAXIDERMIST_4246
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class TaxidermistDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(FRIENDLY, "Oh, hello. Have you got something you want preserving?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> options("Yes please", "Not right now", "What?").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 10 }
                2 -> player(FRIENDLY, "Not right now.").also { stage = 20 }
                3 -> player(ASKING, "What?").also { stage = 30 }
            }

            10 -> npc(FRIENDLY, "Give it to me to look at then.").also { stage = END_DIALOGUE }

            20 -> npcl(FRIENDLY, "Well, you go kill things so I can stuff them, eh?").also { stage = END_DIALOGUE }

            30 -> npcl(FRIENDLY, "If you bring me a monster head or a very big fish, I can preserve it for you so you can mount it in your house. I hear there are all sorts of exotic creatures in the").also { stage++ }
            31 -> npcl(FRIENDLY, "Slayer Tower -- I'd like a chance to stuff one of them!").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(TAXIDERMIST_4246)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return TaxidermistDialogue(player)
    }
}