package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.STONEMASON_4248
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class StonemasonDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(OLD_DEFAULT, "Do you want to buy any stone for building? Or gold leaf?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> options("Yes please", "No thanks", "What kind of stone do you sell?").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(NEUTRAL, "No thanks.").also { stage = END_DIALOGUE }
                3 -> player(ASKING, "What kind of stone do you sell?").also { stage = 10 }
            }

            10 -> npcl(OLD_DEFAULT, "A few kinds. Limestone, of course. Here in Keldagrim we're surrounded by the stuff so I can practically give it away!").also { stage++ }
            11 -> npcl(OLD_DEFAULT, "You'll be able to build some kinds of furniture out of that. Marble is much better, of course, but it costs a lot for me to get hold of it.").also { stage++ }
            12 -> npcl(OLD_DEFAULT, "You can make some beautiful pieces of furniture out of that. I also sell gold leaf, which can be used to decorate furniture made of marble or mahogany.").also { stage++ }
            13 -> npcl(OLD_DEFAULT, "Then for the very special furniture I sell these magic stones. Only the most skilled builders can work them, but they can create a few amazing pieces").also { stage++ }
            14 -> npcl(OLD_DEFAULT, "of furniture with them. So, do you want to buy anything?").also { stage++ }
            15 -> options("Yes please", "No thanks").also { stage++ }
            16 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(NEUTRAL, "No thanks.").also { stage = END_DIALOGUE }
            }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(STONEMASON_4248)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return StonemasonDialogue(player)
    }
}