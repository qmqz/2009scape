package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.JAMILA_5268
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class JamilaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hi. Are you one of the returned townspeople?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(FRIENDLY, "Why, yes. You must be the brave adventurer that Maisa told us was seeking our return.").also { stage++ }
            1 -> player(FRIENDLY, "I guess so.").also { stage++ }
            2 -> options("I'd like to shop, please.", "How is business?", "Any tasks for me that you can think of?", "Goodbye for now.").also { stage++ }
            3 -> when (buttonId) {
                1 -> player(FRIENDLY, "I'd like to shop, please.").also { stage = 10 }
                2 -> player(FRIENDLY, "How is business?").also { stage = 15 }
                3 -> player(FRIENDLY, "Any tasks for me that you can think of?").also { stage = 20 }
                4 -> player(FRIENDLY, "Goodbye for now.").also { stage = 25 }
            }

            10 -> npc(FRIENDLY, "Certainly!").also { stage++ }
            11 -> npc.openShop(player)

            15 -> npcl(WORRIED, "Slow at the moment, so slow. Maybe time will make things better.").also { stage = END_DIALOGUE }

            20 -> npcl(FRIENDLY, "I have no plans that would require your mighty services...at least, not yet.").also { stage = END_DIALOGUE }

            25 -> npc(FRIENDLY, "Come back soon!").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(JAMILA_5268)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return JamilaDialogue(player)
    }
}