package rs09.game.content.dialogue.region.burthorpe.castle

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.RACHAEL_1358
import org.rs09.consts.NPCs.SAM_1357
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BurthorpeBarDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        n(FRIENDLY, "Welcome to the Burthorpe Games Rooms!")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        fun gender (male : String = "mister", female : String = "miss") = if (player.isMale) male else female
        when(stage) {
            0 -> o("How do I play board games?", "What games can I play?", "Do you have any news?", "Can I buy a drink please?", "Thanks!").also { stage++ }
            1 -> when (b) {
                1 -> p(ASKING, "How do I play board games?").also { stage = 10 }
                2 -> p(ASKING, "What games can I play?").also { stage = 20 }
                3 -> p(ASKING, "Do you have any news?").also { stage = 30 }
                4 -> p(ASKING, "Can I buy a drink please?").also { stage = 40 }
                5 -> p(FRIENDLY, "Thanks!").also { stage = END_DIALOGUE }
            }

            10 -> nl(FRIENDLY, "You can challenge someone to a game anywhere in the games rooms by using the right click option. Choose the type of game and then choose the options you want such as time per move.").also { stage++ }
            11 -> nl(FRIENDLY, "If you want to play a particular game there are challenge rooms dedicated to that game.").also { stage++ }
            12 -> nl(FRIENDLY, "In the challenge rooms you can see other players ranks by right clicking them, their skill will be displayed instead of their combat level.").also { stage++ }
            13 -> nl(FRIENDLY, "Once you have enough experience you will be able to use the challenge rooms on the first floor!").also { stage++ }
            14 -> nl(FRIENDLY, "If your opponent accepts the challenge you will be taken to one of the tables in the main room where you will play your game of choice.").also { stage++ }
            15 -> nl(FRIENDLY, "Once you have finished your game you will go back to the challenge room where you can challenge again!").also { stage = 0 }

            20 -> nl(FRIENDLY, "Currently we offer Draughts, Runelink, Runesquares and Runeversi.").also { stage++ }

            21 -> o("Draughts?", "Runelink?", "Runesquares?", "Runeversi?").also { stage++ }
            22 -> when (b) {
                1 -> p(ASKING, "Draughts?").also { stage = 100 }
                2 -> p(ASKING, "Runelink?").also { stage = 200 }
                3 -> p(ASKING, "Runesquares?").also { stage = 300 }
                4 -> p(ASKING, "Runeversi?").also { stage = 400 }
            }

            30 -> nl(FRIENDLY, "There are some notice boards in the main entrance, check them from time to time for the latest news.").also { stage = 0 }

            40 -> nl(FRIENDLY, "Certainly ${gender()}, our specialty is Asgarnian Ale, we also serve Wizard's Mind Bomb and Dwarven Stout.").also { stage++ }
            41 -> npc.openShop(player)

            100 -> nl(FRIENDLY, "Draughts uses standard rules, apart from: a draw is declared if no piece has been taken or promoted for forty moves. To play Draughts go to the challenge room in the SW corner.").also { stage = 0 }
            200 -> nl(FRIENDLY, "Yup, you have to get four runes in a row. The challenge room for Runelink is in the SE corner.").also { stage = 0 }
            300 -> nl(FRIENDLY, "Yes, you take it in turns to add a line with the goal of making squares. Everytime you make a square you take another turn. The challenge room for Runesquares is in the SW corner.").also { stage = 0 }
            400 -> nl(FRIENDLY, "Yep, the aim is to have more of your runes on the board than your opponent. You can take your opponent's pieces by trapping them between your own. The challenge room for Runeversi is in the SE corner.").also { stage = 0 }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(RACHAEL_1358, SAM_1357)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BurthorpeBarDialogue(player)
    }
}