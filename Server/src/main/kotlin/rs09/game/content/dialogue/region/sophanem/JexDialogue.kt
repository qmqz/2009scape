package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.JEX_5279
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class JexDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        //before Contact quest
        npcl(SUSPICIOUS, "What business does the cursed one have with a servant of Icthlarin?").also { stage = 0 }

        //after Contact quest
        //player(FRIENDLY, "Hello, Jex.").also { stage = 10 }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> playerl(FRIENDLY, "I need some help figuring out what's going on.").also { stage++ }
            1 -> npcl(ANNOYED, "I will not aid a defiler of the dead. Get ye gone; your presence is soiling my austere aura.").also { stage = END_DIALOGUE }

            10 -> npc(FRIENDLY, "Hello, ${player.name}.").also { stage++ }
            11 -> sendDialogue("(Jex rubs his head tenderly)").also { stage++ }
            12 -> options("Are you okay?", "Could you tell me more about the minor gods?", "Why is your temple still in such a state?", "Goodbye, Jex.").also { stage++ }
            13 -> when (buttonId) {
                1 -> player(ASKING, "Are you okay?").also { stage = 20 }
                2 -> player(ASKING, "Could you tell me more about the minor gods?").also { stage = 30 }
                3 -> player(ASKING, "Why is your temple still in such a state?").also { stage = 40 }
                4 -> player(FRIENDLY, "Goodbye, Jex.").also { stage = 500 }
            }

            20 -> npcl(ANNOYED, "Some degenerate clubbed me and gave me a lump the size of an egg on the back of my head.").also { stage++ }
            21 -> playerl(WORRIED, "I'll keep my eyes peeled for suspicious looking characters.").also { stage++ }
            22 -> npc(ANNOYED, "I have a fairly good idea who did it.").also { stage++ }
            23 -> player(WORRIED, "Who?").also { stage++ }
            24 -> npcl(SUSPICIOUS, "That linen merchant, Siamun; he's never liked me.").also { stage++ }
            25 -> player(GUILTY, "I'll keep an eye out for him.").also { stage = 12 }

            30 -> options("Could you tell me more about Scabaras?", "Could you tell me more about Het?", "Could you tell me more about Crondis?", "Could you tell me more about Apmeken?", "Goodbye, Jex.").also { stage++ }
            31 -> when (buttonId) {
                1 -> player(ASKING, "Could you tell me more about Scabaras?").also { stage = 100 }
                2 -> player(ASKING, "Could you tell me more about Het?").also { stage = 200 }
                3 -> player(ASKING, "Could you tell me more about Crondis?").also { stage = 300 }
                4 -> player(ASKING, "Could you tell me more about Apmeken?").also { stage = 400 }
                5 -> player(FRIENDLY, "Goodbye, Jex.").also { stage = 500 }
            }

            40 -> npcl(THINKING, "The minor gods aren't particularly revered in our fine city. Icthlarin holds sway here and, although a fine deity indeed, he's not the most jolly of sorts.").also { stage++ }
            41 -> player(ASKING, "I thought you were a priest of Icthlarin?").also { stage++ }
            42 -> npcl(FRIENDLY, "Well, yes and no. I am primarily a priest of Icthlarin, certainly, but I have been trained in the rites of the minor gods as well.").also { stage++ }
            43 -> playerl(ASKING, "What would it take to repair this temple, then? Money? Materials? Manpower?").also { stage++ }
            44 -> npcl(THINKING, "I'm not really sure, to be honest. Most of the natural worshippers are in Menaphos; of course, and the local economy is rather depressed. In addition, that Templeton rogue").also { stage++ }
            45 -> npcl(ANNOYED, "made off with some of the damaged stonework. We should hunt him down like the jackal he is.").also { stage++ }
            46 -> playerl(FRIENDLY, "I'm sure you'll think of something; you'll be able to count on me when you do.").also { stage = 12 }

            100 -> npcl(THINKING, "I think you'll have gathered most of the relevant details from elsewhere, but general knowledge isn't always the most accurate.").also { stage++ }
            101 -> player(ASKING, "Are you claiming I've been misinformed?").also { stage++ }
            102 -> npcl(THINKING, "Not really; there are simply details of Scabaras' fall from grace that are only known to we priests.").also { stage++ }
            103 -> player(ASKING, "Such as?").also { stage++ }
            104 -> npcl(THINKING, "Some of his supposed faults can be forgiven. I just can't explain why at the moment.").also { stage++ }
            105 -> playerl(WORRIED, "I'm not sure whether I should thank you for that bit of confusion or add to that bump on your head.").also { stage = 30 }

            200 -> npcl(THINKING, "Het is the least strange looking of our deities to foreign eyes; he looks just like us. I suspect this is why his shrines are particularly targeted by the infamous Templeton.").also { stage++ }
            201 -> player(ASKING, "What does his patronage cover?").also { stage++ }
            202 -> npcl(FRIENDLY, "He is the very incarnation of health and strength in both mind and body.").also { stage++ }
            203 -> options("Could you tell me more about Het's influence over the desert?", "Could you tell me more about Het's followers?", "Could you tell me more about the minor gods?").also { stage++ }
            204 -> when (buttonId) {
                1 -> playerl(ASKING, "Could you tell me more about Het's influence over the desert?").also { stage = 210 }
                2 -> player(ASKING, "Could you tell me more about Het's followers?").also { stage = 220 }
                3 -> player(ASKING, "Could you tell me more about the minor gods?").also { stage = 30 }
            }

            210 -> npcl(THINKING, "When I was young, I thought of Het as a strong virtuous knight: brave, daring, and, perhaps, just a little dim.").also { stage++ }
            211 -> player(FRIENDLY, "Sounds like every knight I've ever known").also { stage++ }
            212 -> npcl(FRIENDLY, "But, as I grow older, I'm not so sure. It is clear that Het's influence is felt all over the desert; his strength can be felt on the serrated edges of the Menaphites' blades.").also { stage++ }
            213 -> npcl(FRIENDLY, "His nerve is in the tip of every spear wielded by the soldiers of Al Kharid.").also { stage++ }
            214 -> npcl(FRIENDLY, "He fortifies each steel cuff that constrains a slave. He is the very tug in the river Elid that drags many to their deaths.").also { stage++ }
            215 -> npcl(FRIENDLY, "Yet, we have enfeebled slaves and weakened villages. Even in this very city there is a plague sapping us of our strength.").also { stage++ }
            216 -> playerl(ASKING, "So, you feel that Het has turned his back on your people?").also { stage++ }
            217 -> npcl(THINKING, "No, I wouldn't say that. We mortals are too quick to blame our problems on the gods. Without weakness there is no strength, after all.").also { stage = 203 }

            220 -> npcl(FRIENDLY, "Het originated in the north, so he is worshipped fervently by the people of Al Kharid; however, he is a popular god across the desert - his powerful image of strength and health make him a").also { stage++ }
            221 -> npc(FRIENDLY, "popular god amongst soldiers and leaders.").also { stage = 203 }

            300 -> npcl(THINKING, "Crondis is a ferocious lady with the head of a mighty crocodile.").also { stage++ }
            301 -> player(THINKING, "That must be awkward when buying helmets.").also { stage++ }
            302 -> npcl(ANNOYED, "Enough flippancy: remember she is a deity, and this is her place of power.").also { stage++ }
            303 -> player(ASKING, "What kind of people follow her?").also { stage++ }
            304 -> npcl(FRIENDLY, "She embodies modesty, diligence and resourcefulness, so Crondis is worshipped by the desert's subsistence farmers and hunters. Conversely, she proves unpopular amongst society's elite.").also { stage++ }
            305 -> player(ASKING, "How so?").also { stage++ }
            306 -> npcl(FRIENDLY, "Crondis teaches that people should take no more than they need and make do with little, which, as you can imagine, conflicts with the values of many in the richer echelons of society.").also { stage++ }
            307 -> npcl(THINKING, "In Menaphos, she is particularly frowned upon, although that is hardly surprising.").also { stage++ }
            308 -> player(ASKING, "Why?").also { stage++ }
            309 -> npcl(THINKING, "The Pharaoh of Menaphos is a greedy, cruel man whose lack of restraint is beyond even Crondis's help.").also { stage++ }
            310 -> npcl(WORRIED, "Excuse me, I should not have spoken that way. It is unwise to speak ill of one's betters.").also { stage++ }
            311 -> options("Could you tell me more about Crondis's influence over the desert?", "Could you tell me more about the minor gods?").also { stage++ }
            312 -> when (buttonId) {
                1 -> player(ASKING, "Could you tell me more about Crondis's influence?").also { stage = 320 }
                2 -> player(ASKING, "Could you tell me more about the minor gods?").also { stage = 30 }
            }

            320 -> npcl(FRIENDLY, "Crondis hopes to inspire resourcefulness and unassuming modesty. While this has always been her goal, her methods vary more than most.").also { stage++ }
            321 -> player(ASKING, "I'm confused. What do you mean?").also { stage++ }
            322 -> npcl(FRIENDLY, "Crondis may save a struggling harvest by bursting the clouds, or she might set swarms of locusts upon a bountyful crop to stop complacency.").also { stage = 311 }

            400 -> npcl(THINKING, "Apmeken was the most mischievous, playful and unpredictable of the minor deities. My personal favourite, in fact.").also { stage++ }
            401 -> player(SUSPICIOUS, "I'm guessing I shouldn't trust you, then?").also { stage++ }
            402 -> npcl(FRIENDLY, "While Apmeken was mischievous, she was also a goddess of friendship. Like her, I may trip you, but I'd catch you before you fell.").also { stage++ }
            403 -> playerl(ASKING, "Well, that's good to know. I presume she looks slightly inhuman?").also { stage++ }
            404 -> npcl(FRIENDLY, "She had the head of a monkey, ape or baboon, depending on the nature she was displaying at the time. The monkey was skillful, the ape was wise and the baboon was comical.").also { stage++ }
            405 -> playerl(THINKING, "I see. Jex, I can't help notice that you're speaking in the past tense.").also { stage++ }
            406 -> npcl(NEUTRAL, "Ah, sorry, yes. I'm of the opinion - and it's an opinion not shared by many, I should add - that Apmeken has moved on from these lands.").also { stage++ }
            407 -> npcl(FRIENDLY, "I've read reams of scripture detailing Apmeken, and heard endless tales of her deeds. I can't believe that the desert would be such a warring, sombre, plague-filled place if she was still felt here.").also { stage++ }
            408 -> playerl(ASKING, "That's interesting, Jex; can I ask you some more questions about her?").also { stage++ }
            409 -> npc(FRIENDLY, "Of course.").also { stage++ }
            410 -> options("Could you tell me more about Apmeken's connection with monkeys?", "Could you tell me more about Apmeken's followers?", "Could you tell me more about Apmeken's influence over the desert?", "Could you tell me more about the minor gods?").also { stage++ }
            411 -> when (buttonId) {
                1 -> playerl(ASKING, "Could you tell me more about Apmeken's connection with monkeys?").also { stage = 420 }
                2 -> playerl(ASKING, "Could you tell me more about Apmeken's followers?").also { stage = 430 }
                3 -> playerl(ASKING, "Could you tell me more about Apmeken's influence over the desert?").also { stage = 440 }
                4 -> player(ASKING, "Could you tell me more about the minor gods?").also { stage = 30 }

            }

            420 -> npcl(FRIENDLY, "Apmeken was once a monkey. Many people believe that the native monkeys embodied her very spirit. They were her most beloved supporters. There hasn't been any wild monkeys in the desert for a long time - not").also { stage++ }
            421 -> npc(FRIENDLY, "since I was a child.").also { stage++ }
            422 -> npcl(SAD, "I'm afraid that the lack of monkeys living here is only further proof that Apmeken has indeed moved on.").also { stage = 410 }

            430 -> npcl(THINKING, "Apmeken is worshipped by those who value the community and friendship above everything else: mothers, priests and even politicians.").also { stage++ }
            431 -> npcl(FRIENDLY, "Apmeken originated in the west, in the space between two warring tribes, and it is there she found most of her support.").also { stage = 410 }

            440 -> npcl(THINKING, "Apmeken's influence was subtle but incredible, she was an infectious laugh, a jaunty jig, a joke you've heard a hundred times before, but still chuckle at.").also { stage++ }
            441 -> npcl(FRIENDLY, "It may not sound like much, but these subtleties have been known to avert wars and make enemies set aside their differences.").also { stage = 410 }

            500 -> npc(FRIENDLY, "Goodbye. May the gods be with you.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(JEX_5279)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return JexDialogue(player)
    }
}