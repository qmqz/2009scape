package rs09.game.content.dialogue.region.canifis

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.entity.skill.crafting.TanningProduct
import core.plugin.Initializable
import org.rs09.consts.NPCs.SBOTT_1041
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SbottDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(FRIENDLY, "Hello stranger. Would you like me to tan any hides for you?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> options("Yes please.", "No thanks, I'm not interested.").also { stage++ }
            1 -> when (buttonId) {
                1 -> {
                    end()
                    TanningProduct.open(player, 1041)
                }
                2 -> player(FRIENDLY, "No thanks, I'm not interested.?").also { stage = 10 }
            }

            10 -> npcl(ASKING, "Okay; you change your mind, you come see me. I'm your guy!").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SBOTT_1041)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SbottDialogue(player)
    }
}