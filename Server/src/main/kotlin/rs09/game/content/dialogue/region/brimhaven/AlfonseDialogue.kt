package rs09.game.content.dialogue.region.brimhaven

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ALFONSE_THE_WAITER_793
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AlfonseDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(ASKING, "Welcome to the Shrimp and Parrot. Would you like to order, madam?")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when(stage) {
            0 -> options("Yes please.", "No thank you.", "Where do you get your Karambwan from?").also { stage++ }
            1 -> when (b) {
                1 -> player(ASKING, "Yes please.").also { stage = 10 }
                2 -> player(NEUTRAL, "No thank you.").also { stage = END_DIALOGUE }
                3 -> playerl(ASKING, "Where do you get your Karambwan from?").also { stage = 20 }
            }

            10 -> npc.openShop(player)

            20 -> npcl(FRIENDLY, "We buy directly off Lubufu, a local fisherman. He seems to have a monopoly over Karambwan sale.").also { stage++ }

            21 -> options("Where can I find Lubufu?", "How does he manage a monopoly?").also { stage++ }
            22 -> when (b) {
                1 -> player(ASKING, "Where can I find Lubufu?").also { stage = 30 }
                2 -> player(NEUTRAL, "How does he manage a monopoly?").also { stage = 40 }
            }

            30 -> npcl(NEUTRAL, "He is usually working just to the south of the town.").also { stage++ }
            31 -> player(FRIENDLY, "Thanks.").also { stage = END_DIALOGUE }

            40 -> npcl(NEUTRAL, "Simple - nobody else knows how to catch Karambwan. It is a closely guarded secret.").also { stage++ }
            41 -> player(FRIENDLY, "Thanks.").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ALFONSE_THE_WAITER_793)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AlfonseDialogue(player)
    }
}