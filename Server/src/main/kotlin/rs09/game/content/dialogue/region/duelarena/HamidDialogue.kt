package rs09.game.content.dialogue.region.duelarena

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.HAMID_1008
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class HamidDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hi!")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(FRIENDLY, "Hello, traveller. How may I be of assistance?").also { stage++ }
            1 -> options("Can you heal me?", "What's a monk doing in a place like this?", "Which monastery do you come from?").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(ASKING, "Can you heal me?").also { stage = 10 }
                2 -> player(ASKING, "What's a Monk doing in a place such as this?").also { stage = 20 }
                3 -> player(ASKING, "Which monastery do you come from?").also { stage = 30 }
            }

            10 -> npcl(FRIENDLY, "You'd be better off speaking to one of the nurses.").also { stage++ }
            11 -> npc(FRIENDLY, "They are so... nice... afterall!").also { stage = END_DIALOGUE }

            20 -> npcl(FRIENDLY, "Well don't tell anyone but I came here because of the nurses!").also { stage++ }
            21 -> player(ASKING, "Really?").also { stage++ }
            22 -> npc(LAUGH, "It beats being stuck in the monastery!").also { stage = END_DIALOGUE }

            30 -> npcl(FRIENDLY, "I belong to the monastery north of Falador.").also { stage++ }
            31 -> player(ASKING, "You're a long way from home?").also { stage++ }
            32 -> npc(SAD, "Yeh. I miss the guys.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(HAMID_1008)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return HamidDialogue(player)
    }
}