package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.NEUTRAL
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.USI_3031
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class UsiDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY,"Good day to you.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> npc(NEUTRAL, "It's not that good, it's a bit warm.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return UsiDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(USI_3031)
    }
}