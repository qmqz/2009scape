package rs09.game.content.dialogue.region.burthorpe.castle

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SERVANT_1081
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class ServantDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        p(FRIENDLY, "Hi!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> n(FRIENDLY, "Hi!").also { stage++ }
            1 -> n(WORRIED, "Look, I'd better not talk. I'll get in trouble.").also { stage++ }
            2 -> nl(HALF_WORRIED, "If you want someone to show you round the castle ask Eohric, the Head Servant.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SERVANT_1081)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return ServantDialogue(player)
    }
}