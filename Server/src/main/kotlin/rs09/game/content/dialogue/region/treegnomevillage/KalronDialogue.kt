package rs09.game.content.dialogue.region.treegnomevillage

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.OLD_CALM_TALK1
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.KALRON_486
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class KalronDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hello.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(OLD_CALM_TALK1, "Gotta find a way out. We built this maze for protection but I can't get used to it. I'm always getting lost.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(KALRON_486)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return KalronDialogue(player)
    }
}