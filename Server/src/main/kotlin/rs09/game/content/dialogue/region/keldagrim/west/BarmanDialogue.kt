package rs09.game.content.dialogue.region.keldagrim.west

import api.addItemOrDrop
import api.amountInInventory
import api.inInventory
import api.removeItem
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable
import org.rs09.consts.Items.BEER_1917
import org.rs09.consts.Items.COINS_995
import org.rs09.consts.Items.DWARVEN_STOUT4_5777
import org.rs09.consts.Items.JUG_OF_WINE_1993
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BarmanDialogue(player: Player? = null) : DialoguePlugin(player) {

    override fun open(vararg args: Any?): Boolean {
        val gender = if (player.isMale) "sir" else "madam"
        npc = args[0] as NPC

        if (amountInInventory(player, COINS_995) > 2000) {
            npc(OLD_DEFAULT, "And what will it be for you, fine $gender?")
        }

        if (amountInInventory(player, COINS_995) <= 2000) {
            npc(OLD_DEFAULT, "Yes? Can I help you with something?")
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        val gender = if (player.isMale) "sir" else "madam"
        when(stage) {
            0 -> options("A beer please.", "A dwarven stout please.", "A fine wine please.", "Nothing, thanks.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(FRIENDLY, "A beer please.").also { stage = 10 }
                2 -> player(FRIENDLY, "A dwarven stout please.").also { stage = 20 }
                3 -> player(FRIENDLY, "A fine wine please.").also { stage = 30 }
                4 -> player(NEUTRAL, "Nothing, thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npc(OLD_DEFAULT, "That'll be 1 gold coin please.").also { stage++ }

            11 -> options("Pay.", "Don't pay.").also { stage++ }
            12 -> when (buttonId) {
                1 -> {
                    if (!inInventory(player, COINS_995)) {
                        player(WORRIED, "I'm sorry, I seem to have run out of money.").also { stage = END_DIALOGUE }
                    } else {
                        removeItem(player, Item(COINS_995, 1))
                        addItemOrDrop(player, BEER_1917)
                        npc(OLD_DEFAULT, "Here you go. One delicious pint of beer.").also { stage = END_DIALOGUE }
                    }
                }
                2 -> player(NEUTRAL, "Sorry, I changed my mind.").also { stage = END_DIALOGUE }
            }

            20 -> npc(OLD_DEFAULT, "It'll be yours for just 2 gold coins!").also { stage++ }

            21 -> options("Pay.", "Don't pay.").also { stage++ }
            22 -> when (buttonId) {
                1 -> {
                    if (amountInInventory(player, COINS_995) < 2) {
                        player(WORRIED, "I'm sorry, I seem to have run out of money.").also { stage = END_DIALOGUE }
                    } else {
                        removeItem(player, Item(COINS_995, 2))
                        addItemOrDrop(player, DWARVEN_STOUT4_5777)
                        npc(OLD_DEFAULT, "One dwarven stout, best in all of Keldagrim!").also { stage = END_DIALOGUE }
                    }
                }
                2 -> player(NEUTRAL, "Sorry, I changed my mind.").also { stage = END_DIALOGUE }
            }

            30 -> npcl(OLD_DEFAULT, "Ah, $gender has taste! A jug of wine will cost you 10 gold coins.").also { stage++ }

            31 -> options("Pay.", "Don't pay.").also { stage++ }
            32 -> when (buttonId) {
                1 -> {
                    if (amountInInventory(player, COINS_995) < 10) {
                        playerl(WORRIED, "I'm sorry, I seem to have run out of money.").also { stage = END_DIALOGUE }
                    } else {
                        removeItem(player, Item(COINS_995, 10))
                        addItemOrDrop(player, JUG_OF_WINE_1993)
                        npc(OLD_DEFAULT, "I hope $gender will enjoy it!").also { stage = END_DIALOGUE }
                    }
                }
                2 -> player(NEUTRAL, "Sorry, I changed my mind.").also { stage = END_DIALOGUE }
            }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.BARMAN_2179)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BarmanDialogue(player)
    }
}