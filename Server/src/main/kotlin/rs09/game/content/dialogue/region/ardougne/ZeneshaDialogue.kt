package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ZENESHA_589
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class ZeneshaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(FRIENDLY, "Hello there! I sell plate mail bodies, are you interested?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("I'm interested.", "Sorry, I'm not interested.").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> npcl(FRIENDLY, "Sorry, I'm not interested.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ZENESHA_589)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return ZeneshaDialogue(player)
    }
}