package rs09.game.content.dialogue.region.varrock.palace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.AMBASSADOR_FERRNOOK_4582
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AmbassadorFerrnookDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        playerl(FRIENDLY, "Hello Ambassador. Are you here visiting King Roald?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(OLD_CALM_TALK1, "Well, in theory, but he always seems to be busy.").also { stage++ }
            1 -> playerl(ASKING, "You don't seem that upset by that, though...").also { stage++ }
            2 -> npcl(OLD_CALM_TALK2, "Oh no, I like travelling, and if you become a diplomat patience is a vital skill.").also { stage++ }
            3 -> player(FRIENDLY, "I'll try to remember that.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(AMBASSADOR_FERRNOOK_4582)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AmbassadorFerrnookDialogue(player)
    }
}