package rs09.game.content.dialogue.region.falador

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.NARF_3238
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class NarfDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FacialExpression.FRIENDLY,"That's a funny name you've got.").also { stage = 0 }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> npcl(FacialExpression.ANNOYED, "'Narf'? You think that's funny? At least I don't call myself ${player.name}! Where did you get a name like that?").also { stage++ }
            1 -> player(FacialExpression.GUILTY, "It seemed like a good idea at the time!").also { stage++ }
            2 -> npc(FacialExpression.ANNOYED, "Bah!").also { stage = END_DIALOGUE }

            99 -> end()
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return NarfDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NARF_3238)
    }
}