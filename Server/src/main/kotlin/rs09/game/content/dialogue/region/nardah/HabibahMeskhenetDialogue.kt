package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.HABIBAH_3034
import org.rs09.consts.NPCs.MESKHENET_3035
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class HabibahMeskhenetDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY,"Good day to you.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> npc(WORRIED, "Hello.").also { stage++ }
            1 -> player(HALF_ASKING, "You don't look too happy.").also { stage++ }
            2 -> npc(WORRIED, "True. We've not fallen on the best of times here.").also { stage++ }
            3 -> player(ASKING, "Any way that I can help?").also { stage++ }
            4 -> npcl(WORRIED, "Possibly. I'd go talk to Awusah the Mayor of Nardah. He's in the big house on the east side of the town square.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return HabibahMeskhenetDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(HABIBAH_3034, MESKHENET_3035)
    }
}