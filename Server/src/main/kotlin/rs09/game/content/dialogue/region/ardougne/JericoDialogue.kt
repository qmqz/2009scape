package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.JERICO_366
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class JericoDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY, "Hello.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(ASKING, "Can I help you?").also { stage++ }
            1 -> player(NEUTRAL, "Just passing by.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(JERICO_366)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return JericoDialogue(player)
    }
}