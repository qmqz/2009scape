package rs09.game.content.dialogue.region.varrock.palace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.AEONISIG_RAISPHER_4710
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AeonisigDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        playerl(ASKING,"Please only talk to the King if it's important. He has a heavy burden to bear with the running of his Kingdom.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("Who are you?", "What do you do here?", "Where did you come from?", "How did you come to be an advisor to King Roald?", "Okay, thanks.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(ASKING, "Who are you?").also { stage = 10 }
                2 -> player(ASKING, "What do you do here?").also { stage = 20 }
                3 -> playerl(ASKING, "Where did you come from?").also { stage = 30 }
                4 -> playerl(ASKING, "How did you come to be an advisor to King Roald?").also { stage = 40 }
                5 -> player(FRIENDLY, "Okay, thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npcl(FRIENDLY, "Apologies! Allow me to introduce myself: my name is Aeonisig Raispher, special advisor to King Roald on spiritual matters.").also { stage++ }
            11 -> playerl(ASKING, "Special advisor on spiritual matters? What does that mean?").also { stage++ }
            12 -> npcl(FRIENDLY, "It means that some decisions the King has to make might have unforeseen repercussions on the nation's spiritual sensibilities. My duty is to ensure that Saradominist ideals are not stomped underfoot.").also { stage = 0 }

            20 -> npcl(FRIENDLY, "My main function is to ensure that King Roald is apprised of all options, especially those that favour the righteous followers of Saradomin.").also { stage++ }
            21 -> playerl(ASKING, "But surely the King should be able to make his own decisions on what's best for Misthalin?").also { stage++ }
            22 -> npcl(FRIENDLY, "What an interesting perspective you have! Totally unworkable of course, but interesting nonetheless.").also { stage = 0 }

            30 -> npcl(FRIENDLY, "I took my religious and combat training in several parts of the known world. I've also fought despicable beasts in the wilderness in Saradomin's name. Needless to say I have great experience in the ways of the world and am").also { stage++ }
            31 -> npcl(FRIENDLY, "an invaluable aid to his lordship's decision-making process.").also { stage = 0 }

            40 -> npcl(FRIENDLY, "The King of Misthalin, like any great leader, always requires the best advice and the best advisors. He very often summons occasional advisors to help him in certain situations, but it was felt by the Church of Saradomin").also { stage++ }
            41 -> npcl(FRIENDLY, "that a full time advisor on religious matters was needed to ensure fair treatment of Saradomin's followers.").also { stage++ }
            42 -> playerl(ASKING, "How come he doesn't have an advisor for any other religious denominations?").also { stage++ }
            43 -> npcl(FRIENDLY, "Because I simply won't stand for it, that's why! Now, enough of your impertinent questions. I have work to do!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AeonisigDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(AEONISIG_RAISPHER_4710)
    }
}
