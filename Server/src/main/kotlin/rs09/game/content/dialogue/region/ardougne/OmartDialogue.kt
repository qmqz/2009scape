package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.OMART_350
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class OmartDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY, "Hello there.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(FRIENDLY, "Hello.").also { stage++ }
            1 -> player(FRIENDLY, "How are you?").also { stage++ }
            2 -> npc(FRIENDLY, "Fine thanks.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(OMART_350)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return OmartDialogue(player)
    }
}