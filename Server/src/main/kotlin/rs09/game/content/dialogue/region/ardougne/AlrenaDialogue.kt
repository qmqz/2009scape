package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ALRENA_710
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AlrenaDialogue(player: Player? = null) : DialoguePlugin(player) {


    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY, "Hello Madam.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(SAD, "Oh, hello there.").also { stage++ }
            1 -> player(ASKING, "Are you ok?").also { stage++ }
            2 -> npcl(SAD, "Not too bad... I've just got some troubles on my mind...").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ALRENA_710)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AlrenaDialogue(player)
    }
}