package rs09.game.content.dialogue.region.keldagrim.palace.marketplace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ODMAR_2200
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class OdmarDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(OLD_NORMAL,"What to buy? What to buy? Oh, hello human.")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when(stage){
            0 -> p(ASKING, "Anything I can help you with?").also { stage++ }
            1 -> nl(OLD_CALM_TALK1, "Only if you can help me make a suggestion on what to buy.").also { stage++ }
            2 -> p(FRIENDLY, "I smell a quest!").also { stage++ }
            3 -> nl(OLD_CALM_TALK2, "No, not really. I just need some advice on what to buy, that's all.").also { stage++ }

            4 -> o("New clothes.", "A warhammer.", "A piece of rope.", "An oversized hat.", "Nothing.").also { stage++ }
            5 -> when (b) {
                1 -> pl(FRIENDLY, "You should buy some new clothes. You can't go around town wearing what you're wearing.").also { stage = 10 }
                2 -> pl(FRIENDLY, "You should get yourself a big mighty warhammer.").also { stage = END_DIALOGUE }
                3 -> p(FRIENDLY, "Get yourself a long piece of rope.").also { stage = 20 }
                4 -> pl(FRIENDLY, "An oversized hat. In a ridiculous colour.").also { stage = 30 }
                5 -> pl(FRIENDLY, "Nothing. You should buy absolutely nothing.").also { stage = 40 }
            }

            10 -> nl(OLD_CALM_TALK1, "You're right, this is a little drab. I shouldn't really go around Keldagrim-West wearing this, should I? Of course I still don't know what colour I should get.").also { stage++ }
            11 -> nl(OLD_CALM_TALK2, "I don't want to accidentally offend the wrong company of course.").also { stage++ }
            12 -> p(THINKING, "Never mind.").also { stage = END_DIALOGUE }

            20 -> nl(OLD_CALM_TALK1, "What would I do with a rope?").also { stage++ }
            21 -> pl(HALF_THINKING, "I don't know. I generally find a rope useful while adventuring.").also { stage++ }
            22 -> nl(OLD_NOT_INTERESTED, "I don't do a lot of adventuring to be honest.").also { stage = END_DIALOGUE }

            30 -> nl(OLD_NOT_INTERESTED, "No one is selling hats here, that's not a very helpful suggestion at all.").also { stage = END_DIALOGUE }

            40 -> nl(OLD_LAUGH1, "Now that's the best idea I've heard all day.").also { stage++ }
            41 -> pl(HALF_ASKING, "I thought so! Now why don't you give me some of the money I saved you?").also { stage++ }
            42 -> nl(OLD_NOT_INTERESTED, "No, I don't think so. I think I'd rather buy something with my money after all.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return OdmarDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(ODMAR_2200)
    }
}