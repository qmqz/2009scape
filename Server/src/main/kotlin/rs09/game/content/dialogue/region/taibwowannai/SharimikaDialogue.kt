package rs09.game.content.dialogue.region.taibwowannai

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SHARIMIKA_2505
import org.rs09.consts.NPCs.SHARIMIKA_2506
import org.rs09.consts.NPCs.SHARIMIKA_2507
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SharimikaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> when (buttonId) {
                1 -> player(ASKING, "What do you do here?").also { stage = 10 }
                2 -> player(ASKING, "Is there anything interesting to do around here?").also { stage = 200 }
                3 -> player(FRIENDLY, "Ok, thanks.").also { stage = END_DIALOGUE }
            }
            1 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }

            10 -> npcl(FRIENDLY, "I's not doing a great deal now Bwana, as I'm no spring chicken. I do my fair share around the village bwana, though, I could tell you a thing or two about the jungle slashing.").also { stage++ }

            11 -> options("Ok, tell me about jungle slashing!", "Previous questions.").also { stage++ }
            12 -> when (buttonId) {
                1 -> player(FRIENDLY, "Ok, tell me about jungle slashing!").also { stage = 100 }
                2 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }
            }

            100 -> npcl(FRIENDLY, "Ok Bwana, I will! To begin with you'll need a machete in order to hack your way through the jungle. There are some better gem edged weapons which you may be able to purchase from Gabooty.").also { stage++ }
            101 -> npcl(FRIENDLY, "Safta Doc makes them for him. You should be aware that hacking away at the jungle is not without its dangers though Bwana. Many times you may find some scary creatures lurking inside.").also { stage++ }
            102 -> player(ASKING, "What sort of creatures?").also { stage++ }
            103 -> npcl(FRIENDLY, "Ok, to start with there are spiders! These are pretty tough but if you defeat it, you can get a good meal from it, if you know how to cook it that is. My wife cooks them to a turn - Mmmm, delicious.").also { stage++ }
            104 -> player(AMAZED, "Ok go on!").also { stage++ }
            105 -> npcl(FRIENDLY, "Yes Bwana, of course. Then there are snakes...these are dangerous because they're poisonous - so be prepared! However, they're not without their uses. A bush snake is large enough to have a useful hide.").also { stage++ }
            106 -> npcl(FRIENDLY, "Once the hide is tanned you get a snakeskin and you're able to make things from it, the same way that mainlanders use normal leather! However, it's actually still quite small.").also { stage++ }
            107 -> npcl(THINKING, "You may need several skins in order to make something useful.").also { stage++ }
            108 -> player(HALF_THINKING, "Hmm, sounds interesting!").also { stage++ }
            109 -> npcl(FRIENDLY, "Then there are giant mosquitoes bwana, they're fast and agile. You'll need to be quick if you're to hit them at all! However, it certainly does train your reactions.").also { stage = 129 }
            129 -> npcl(FRIENDLY, "They're not very tough, just difficult to hit.").also { stage = 110 }

            110 -> npcl(NEUTRAL, "I also believe that their proboscis is useful in someway, I believe Timfraku's sons sometimes use them for hunting.").also { stage++ }
            111 -> player(ASKING, "Ok, that sounds helpful...anything else?").also { stage++ }
            112 -> npcl(ANNOYED, "Yes Bwana, much much more! What next...Oh yes, the tribesmen...you've probably seen them scouting around the village, sometimes sneaking inside the enclosure to steal our chickens!").also { stage++ }
            113 -> npcl(WORRIED, "They sometimes hide in the jungle bushes waiting for an opportunity to strike when we least expect it! Take care Bwana, these treacherous jungle natives use poison on their spears!").also { stage++ }
            114 -> playerl(HALF_THINKING, "Right Ok, good advice... 'poison on spears....' Anything else I should know about?").also { stage++ }
            115 -> npcl(GUILTY, "Yes Bwana... and this is serious... The jungle is very ancient and many unspeakable acts have been witnessed on these lands. Bizarre supernatural curses were placed on criminals and then").also { stage = 130 }
            130 -> npcl(GUILTY, "they were buried alive!").also { stage = 116 }

            116 -> npcl(GUILTY, "These strange creatures have the curse of the Broodoo on them! They'll rise from their grave with their heads in their chests and move like crazy spirits of the afterlife.").also { stage++ }
            117 -> npcl(WORRIED, "If they're disturbed they will attack anything in the area with their magical abilities! They suffer little from being attacked as they seem to be protected by some form of defence.").also { stage++ }
            118 -> npcl(FRIENDLY, "However... it is said that the means of their death provides the clue to their destruction. So far three distinct Broodoo have been identified. The Broodoo with a green hued skin is said to").also { stage = 131 }
            131 -> npc(SUSPICIOUS, "have died from poison.").also { stage = 119 }

            119 -> npcl(WORRIED, "There is a broodoo of yellow hue and a pale skinned broodoo, but little is known of these two!").also { stage++ }
            120 -> player(WORRIED, "Erk! Well that sounds pretty scary!").also { stage++ }
            121 -> npc(GUILTY, "Yes Bwana...").also { stage++ }
            122 -> player(GUILTY, "Ok, thanks a lot....").also { stage++ }
            123 -> npc(SUSPICIOUS, "Wait Bwana, there's more!").also { stage++ }
            124 -> player(ANNOYED, "More... Blimey! Ok carry on...").also { stage++ }
            125 -> npcl(FRIENDLY, "Some of the local villagers have found strange plants which grow here-abouts. If you cut away at the jungle and find one, it's considered a blessing, though the useful part of the plant is").also { stage = 132 }
            132 -> npc(NEUTRAL, "usually buried underground.").also { stage = 126 }

            126 -> npcl(FRIENDLY, "And also, you may find an uncommon rock type which is impregnated with mineral wealth! Well that concludes all the help I can give you regarding the jungle in Tai Bwo Wannai!").also { stage++ }
            127 -> playerl(FRIENDLY, "I have to say, that's quite a lot of information...thanks very much!").also { stage++ }
            128 -> npc(FRIENDLY, "You're more than welcome bwana!").also { stage = END_DIALOGUE }


            200 -> when ((1..5).random()) {
                1 -> npcl(ANNOYED, "Bwana, if anyone else asks me that question today, I'll explode! Go and ask someone else!").also { stage = END_DIALOGUE }
                2 -> npcl(FRIENDLY, "Not that I'm aware of, just sit back and enjoy the sunshine bwana!").also { stage = END_DIALOGUE }
                3 -> npcl(NEUTRAL, "Sorry Bwana, I'm already busy, why not go and talk to Murcaily! He's around the village somewhere.").also { stage = END_DIALOGUE }
                4 -> npcl(NEUTRAL, "Well, I think that there's some work to be done...perhaps Murcaily can help you. He usually tends to the hardwood grove to the east of Trufitus's hut.").also { stage = END_DIALOGUE }
                5 -> npcl(FRIENDLY, "It's just village life as normal around here Bwana, always something interesting to find to occupy your time if you look hard enough.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SHARIMIKA_2505, SHARIMIKA_2506, SHARIMIKA_2507)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SharimikaDialogue(player)
    }
}