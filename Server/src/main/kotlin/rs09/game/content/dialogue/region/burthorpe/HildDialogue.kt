package rs09.game.content.dialogue.region.burthorpe

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.HILD_1090
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class HildDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        p(FRIENDLY, "Hi!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> {
                when ((1..12).random()) {
                    1 -> n(ANGRY, "Let me at 'em!").also { stage = END_DIALOGUE }
                    2 -> n(ANNOYED, "Trolls? Schmolls!").also { stage = END_DIALOGUE }
                    3 -> n(SCARED, "The trolls are coming!").also { stage = END_DIALOGUE }
                    4 -> n(SAD, "The trolls took my baby son!").also { stage = END_DIALOGUE }
                    5 -> n(FRIENDLY, "Welcome to Burthorpe!").also { stage = 10 }
                    6 -> n(SCARED, "Trolls!").also { stage = END_DIALOGUE }
                    7 -> n(FRIENDLY, "Hello stranger.").also { stage = END_DIALOGUE }
                    8 -> n(ANNOYED, "Go away!").also { stage = END_DIALOGUE }
                    9 -> n(SCARED, "Run!").also { stage = END_DIALOGUE }
                    10 -> n(HAPPY, "Hi!").also { stage = END_DIALOGUE }
                    11 -> nl(SCARED, "The Imperial Guard can no longer protect us!").also { stage = END_DIALOGUE }
                    12 -> nl(HAPPY, "The White Knights will soon have control!").also { stage = END_DIALOGUE }
                }
            }

            10 -> p(FRIENDLY, "Thanks!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(HILD_1090)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return HildDialogue(player)
    }
}