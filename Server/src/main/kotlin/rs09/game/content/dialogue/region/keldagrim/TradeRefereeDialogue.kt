package rs09.game.content.dialogue.region.keldagrim

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.OLD_DEFAULT
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.TRADE_REFEREE_2127
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class TradeRefereeDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        when ((1..5).random()) {
            1 -> npcl(OLD_DEFAULT, "Talk to the secretaries, not to me!").also { stage = END_DIALOGUE }
            2 -> npcl(OLD_DEFAULT, "Clear out!").also { stage = END_DIALOGUE }
            3 -> npcl(OLD_DEFAULT, "Stay out of the octagon center!").also { stage = END_DIALOGUE }
            4 -> npcl(OLD_DEFAULT, "I can't help you!").also { stage = END_DIALOGUE }
            5 -> npcl(OLD_DEFAULT, "Can't talk to you, too busy!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(TRADE_REFEREE_2127)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return TradeRefereeDialogue(player)
    }
}