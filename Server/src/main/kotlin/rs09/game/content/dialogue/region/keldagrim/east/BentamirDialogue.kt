package rs09.game.content.dialogue.region.keldagrim.east

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BENTAMIR_2192
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BentamirDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(OLD_DEFAULT,"Do you mind? You're in my home.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> player(ASKING, "I'm sorry, should I have knocked?").also { stage++ }
            1 -> npc(OLD_DEFAULT, "Very funny, human.").also { stage++ }
            2 -> npcl(OLD_CALM_TALK1, "We can't all live in plush houses, you know. But that doesn't mean us mining dwarves don't work hard.").also { stage++ }
            3 -> player(ASKING, "Where do you do your mining?").also { stage++ }
            4 -> npcl(OLD_CALM_TALK1, "Normally the coal mine to the north. We need a lot of coal to keep our steam engines going, you know.").also { stage++ }
            5 -> player(ASKING, "Can I do a bit of mining there as well?").also { stage++ }
            6 -> npcl(OLD_DEFAULT, "I'm not sure, but as long as no one notices I don't think anyone is going to care.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BentamirDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(BENTAMIR_2192)
    }
}