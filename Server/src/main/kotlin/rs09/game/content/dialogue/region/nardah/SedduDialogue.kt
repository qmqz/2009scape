package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SEDDU_3038
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SedduDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(ASKING,"I buy and sell adventurer's equipment, do you want to trade?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("Yes please.", "No thank you.").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(FRIENDLY, "No thank you.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SedduDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(SEDDU_3038)
    }
}