package rs09.game.content.dialogue.region.keldagrim

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.OLD_DEFAULT
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.TICKET_DWARF_5886
import org.rs09.consts.NPCs.TICKET_DWARF_5887
import org.rs09.consts.NPCs.TICKET_DWARF_5888
import org.rs09.consts.NPCs.TICKET_DWARF_5889
import org.rs09.consts.NPCs.TICKET_DWARF_5890
import org.rs09.consts.NPCs.TICKET_DWARF_5891
import org.rs09.consts.NPCs.TICKET_DWARF_5892
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class TicketDwarfDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(ASKING, "Can I buy a ticket?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(OLD_DEFAULT, "There's no need. Under our agreement with the Dorgeshuun, you get free train travel in return for services rendered.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(TICKET_DWARF_5886, TICKET_DWARF_5887, TICKET_DWARF_5888, TICKET_DWARF_5889,
            TICKET_DWARF_5890, TICKET_DWARF_5891, TICKET_DWARF_5892)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return TicketDwarfDialogue(player)
    }
}