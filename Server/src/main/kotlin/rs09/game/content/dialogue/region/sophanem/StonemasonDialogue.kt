package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.STONEMASON_5262
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class StonemasonDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hello there.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(ASKING, "Hello. What can I do for you?").also { stage++ }
            1 -> player(THINKING, "I'm not sure. Do you have anything for sale?").also { stage++ }
            2 -> npcl(WORRIED, "I'm afraid not. All the recent chaos has caused a lot of supply issues. It will be a long time before things are properly back to normal.").also { stage++ }
            3 -> player(NEUTRAL, "Oh, that's a shame. Never mind then.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(STONEMASON_5262)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return StonemasonDialogue(player)
    }
}