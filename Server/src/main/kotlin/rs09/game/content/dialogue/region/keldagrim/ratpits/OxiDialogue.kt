package rs09.game.content.dialogue.region.keldagrim.ratpits

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.OXI_2993
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class OxiDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        p(ASKING,"Hello. What are you doing?")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when(stage){
            0 -> nl(OLD_CALM_TALK1, "I'm the rat keeper, it's my job to look after these here rats.").also { stage++ }
            1 -> pl(ASKING, "You do know that there are some rats running loose?").also { stage++ }
            2 -> nl(OLD_CALM_TALK2, "Some nibble their way out from time to time. Don't worry there's enough cats down here to sort anything that gets out.").also { stage++ }
            3 -> p(ASKING, "So how do these rat pits work?").also { stage++ }
            4 -> nl(OLD_CALM_TALK2, "The Keldagrim pits cater for those with the larger variety of cats.").also { stage++ }
            5 -> nl(OLD_CALM_TALK1, "If you have an overgrown cat and a couple of coins you can challenge someone else who also has an overgrown cat and some spare change.").also { stage++ }
            6 -> p(FRIENDLY, "That sounds simple enough so far.").also { stage++ }
            7 -> nl(OLD_CALM_TALK1, "You then agree on a wager. It's rumoured that some ratcatchers can talk to their own cats and give them tactics to aid them.").also { stage++ }
            8 -> p(ASKING, "So then how do you win the challenge?").also { stage++ }
            9 -> nl(OLD_CALM_TALK2, "I was just getting to that. Both the challengers' cats are placed inside the arena, and whichever kills 9 rats first wins.").also { stage++ }

            10 -> o("That sounds vile and cruel.", "It sounds a little dangerous for my cat.", "Wow that sounds like fun.", "Is there a limit to how much you can bet?", "Can I challenge you and your cat?").also { stage++ }
            11 -> when (b) {
                1 -> p(WORRIED, "That sounds vile and cruel.").also { stage = 100 }
                2 -> p(WORRIED, "It sounds a little dangerous for my cat.").also { stage = 200 }
                3 -> p(HAPPY, "Wow that sounds like fun!").also { stage = 300 }
                4 -> pl(ASKING, "Is there a limit to how much you can bet?").also { stage = 400 }
                5 -> p(ASKING, "Can I challenge you and your cat?").also { stage = 500 }
            }

            50 -> o("One more thing...", "Thanks for your help.").also { stage++ }
            51 -> when (b) {
                1 -> close().also { o("That sounds vile and cruel.", "It sounds a little dangerous for my cat.", "Wow that sounds like fun.", "Is there a limit to how much you can bet?", "Can I challenge you and your cat?").also { stage = 11 } }
                2 -> p(FRIENDLY, "Thanks for your help.").also { stage = END_DIALOGUE }
            }

            100 -> nl(OLD_CALM_TALK1, "It's a cruel world and the pits are no exception.").also { stage++ }
            101 -> n(OLD_CALM_TALK2, "Each to their own, I say.").also { stage =  50}

            200 -> nl(OLD_SAD, "I agree. Cats have been known to be killed in fights.").also { stage++ }
            201 -> pl(SAD, "Oh how terrible. Is there nothing you can do to save your cat?").also { stage++ }
            202 -> nl(OLD_CALM_TALK1, "Of course there is, you can instruct it to be cautious, and it will leave once it feels as if it is in danger.").also { stage++ }
            203 -> p(FRIENDLY, "Oh that sounds quite wise.").also { stage++ }
            204 -> nl(OLD_CALM_TALK2, "That depends on how you value your cat.").also { stage++ }
            205 -> p(ASKING, "Sorry, I don't follow.").also { stage++ }
            206 -> nl(OLD_CALM_TALK1, "Well if your cat dies or it runs away you lose.").also { stage++ }
            207 -> nl(OLD_CALM_TALK2, "So the longer it stays in the fight the better chance you have of winning.").also { stage = 50 }

            300 -> n(OLD_NOT_INTERESTED, "I don't know about that.").also { stage++ }
            301 -> nl(OLD_CALM_TALK1, "One thing I know though is that it's a quick way to make and lose money.").also { stage = 50 }

            400 -> nl(OLD_CALM_TALK1, "Hmmm... let me think. I don't think that's ever arisen but I think it's 20,000.").also { stage++ }
            401 -> pl(THINKING, "Hmm, that sounds like a silly amount of money to bet on a cat fight.").also { stage++ }
            402 -> nl(OLD_CALM_TALK2, "You'd be surprised what some people would be willing to bet.").also { stage++ }
            403 -> nl(OLD_CALM_TALK2, "There is a minimum bet of 100 coins too.").also { stage = 50 }

            500 -> nl(OLD_CALM_TALK1, "No, not now, we don't really bet with outsiders.").also { stage++ }
            501 -> pl(ASKING, "But I'm not an outsider, am I not inside in the pits now?").also { stage++ }
            502 -> nl(OLD_CALM_TALK2, "I don't know, maybe once you've done something to win over my trust.").also { stage++ }
            503 -> p(ASKING, "Can it be bought?").also { stage++ }
            504 -> n(OLD_DISTRESSED, "What?").also { stage++ }
            505 -> p(GUILTY, "Your trust.").also { stage++ }
            506 -> nl(OLD_CALM_TALK1, "No. Regular townsfolk don't like us, which leads us to distrust them and anyone else.").also { stage = 50 }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return OxiDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(OXI_2993)
    }
}