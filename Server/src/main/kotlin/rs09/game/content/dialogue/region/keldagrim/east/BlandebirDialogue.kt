package rs09.game.content.dialogue.region.keldagrim.east

import api.*
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable
import org.rs09.consts.Items.ALE_YEAST_5767
import org.rs09.consts.Items.COINS_995
import org.rs09.consts.Items.EMPTY_POT_1931
import org.rs09.consts.NPCs.BLANDEBIR_2321
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BlandebirDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        playerl(FRIENDLY, "Hello, I wonder if you could help me on this whole brewing thing...")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(OLD_CALM_TALK1, "I might be able to - what do you need to know?").also { stage++ }

            1 -> options("How do I brew ales?", "How do I brew cider?", "What do I do once my ale has matured?", "Do you have any spare ale yeast?", "That's all I need to know, thanks.").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(ASKING, "How do I brew ales?").also { stage = 10 }
                2 -> player(ASKING, "How do I brew cider?").also { stage = 20 }
                3 -> player(ASKING, "What do I do once my ale has matured?").also { stage = 30 }
                4 -> player(ASKING, "Do you have any spare ale yeast?").also { stage = 40 }
                5 -> player(FRIENDLY, "That's all I need to know, thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npcl(OLD_CALM_TALK1, "Well first off you need to fill the vat with water - two bucketfuls should do the trick. Then you'll need to put in two handfuls of barley malt - that's roasted barley by the way.").also { stage++ }
            11 -> npcl(OLD_CALM_TALK2, "After that you'll be putting your main ingredient in - this will decide which ale it is you're brewing. There should be some good guides around with recipes in.").also { stage++ }
            12 -> npcl(OLD_CALM_TALK1, "Lastly you pour a pot full of ale yeast in, which'll start it off fermenting. Then all you have to do is wait for the good stuff.").also { stage = 1 }

            20 -> npcl(OLD_CALM_TALK1, "First you'll need some apples. You'll need to crush them using this cider-press - four apples should make a full bucket of apple-mush. Take four buckets of mush and fill up the vat.").also { stage++ }
            21 -> npcl(OLD_CALM_TALK2, "After that you pour a pot full of ale yeast in, which'll start it off fermenting. Then all you have to do is wait for the good stuff.").also { stage = 1 }

            30 -> npcl(OLD_CALM_TALK2, "Well, once you've got a full vat of the good stuff, just turn the valve and your barrel will fill up with eight pints of whatever your chosen tipple is. Mind it's an empty barrel, though.").also { stage = 1 }

            40 -> npcl(OLD_CALM_TALK1, "Well, as a matter of fact I do, although I wouldn't describe it as spare. This ale yeast I've got is the best money can buy, but if you've got a pot I'll fill it for you for 25GP - very cheap as it happens.").also { stage++ }
            41 -> {
                if (!inInventory(player, EMPTY_POT_1931) && amountInInventory(player, COINS_995) < 25) {
                    playerl(GUILTY, "I don't have an empty pot, or enough money I'm afraid.").also { stage = 1 }
                }

                if (!inInventory(player, EMPTY_POT_1931) && amountInInventory(player, COINS_995) >= 25) {
                    playerl(GUILTY, "I don't have an empty pot I'm afraid.").also { stage = 1 }
                }

                if (inInventory(player, EMPTY_POT_1931) && amountInInventory(player, COINS_995) < 25) {
                    playerl(GUILTY, "I don't have enough money I'm afraid.").also { stage = 1 }
                }

                if (inInventory(player, EMPTY_POT_1931) && amountInInventory(player, COINS_995) >= 25) {
                    options("That's a good deal - please fill my pot with ale yeast for 25GP.", "No, that's too much I'm afraid.").also { stage++ }
                }
            }

            42 -> when(buttonId) {
                1 -> playerl(FRIENDLY, "That's a good deal - please fill my pot with ale yeast for 25GP.").also { stage = 50 }
                2 -> player(GUILTY, "No, that's too much I'm afraid.").also { stage = 1 }
            }

            50 -> {
                removeItem(player, Item(COINS_995, 25))
                removeItem(player, EMPTY_POT_1931)
                addItemOrDrop(player, ALE_YEAST_5767)
                sendItemDialogue(player, ALE_YEAST_5767, "You receive an Ale yeast.").also { stage = 1 }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(BLANDEBIR_2321)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BlandebirDialogue(player)
    }
}