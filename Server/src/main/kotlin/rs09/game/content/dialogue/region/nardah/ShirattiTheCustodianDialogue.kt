package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SHIRATTI_THE_CUSTODIAN_3044
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class ShirattiTheCustodianDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        options("Good day to you.", "What is this building?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> when (buttonId) {
                1 -> npcl(WORRIED, "Be Careful in here and don't touch anything! There's some old and valuable artefacts around here. Careless outsiders.. you're always coming in and making a mess of things!").also { stage = 10 }
                2 -> npc(FRIENDLY, "This is our local shrine to She.").also { stage = 20 }
            }

            10 -> player(ANNOYED, "Ok calm down, I'm not going to touch anything.").also { stage++ }
            11 -> npc(WORRIED, "Yes, good don't.").also { stage = END_DIALOGUE }

            20 -> player(ASKING, "What do you mean by She?").also { stage = END_DIALOGUE }
            21 -> npcl(ANNOYED, "Such ignorance! I suppose I should come to expect it. People are forgetting the old ways of the desrt around here. But then.. What do those beuraeucratic fools in Menaphos expect!").also { stage++ }
            22 -> npcl(ANNOYED, "If they're going to take so long to send us a new pirestess of She. Do you know we haven't had one for five years now!").also { stage++ }
            23 -> player(ASKING, "Errm you still haven't told me what you mean by She.").also { stage++ }
            24 -> npcl(ANNOYED, "She! Otherwise known as Elidinis, Goddess of growth and fertility, Lady of the river, Wife of Tumeken, Flower of the desert, Mother of Itchlarin.").also { stage++ }
            25 -> player(THINKING, "That all sounds very confusing, I might just call her Elidinis for short.").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SHIRATTI_THE_CUSTODIAN_3044)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return ShirattiTheCustodianDialogue(player)
    }
}