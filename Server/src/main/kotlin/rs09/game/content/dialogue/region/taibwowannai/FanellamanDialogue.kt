package rs09.game.content.dialogue.region.taibwowannai

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.FANELLAMAN_2523
import org.rs09.consts.NPCs.FANELLAMAN_2524
import org.rs09.consts.NPCs.FANELLAMAN_2525
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class FanellamanDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> when (buttonId) {
                1 -> player(ASKING, "What do you do here?").also { stage = 10 }
                2 -> player(ASKING, "Is there anything interesting to do around here?").also { stage = 20 }
                3 -> player(FRIENDLY, "Ok, thanks.").also { stage = END_DIALOGUE }
            }
            1 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }

            10 -> npcl(FRIENDLY, "Ha, well I'm retired now, but in my heyday I was quite the expert fisherman! These days I just help out around the village where I can, my old bones don't allow me to do any more than that.").also { stage = 1 }

            20 -> when ((1..5).random()) {
                1 -> npcl(ANNOYED, "Bwana, if anyone else asks me that question today, I'll explode! Go and ask someone else!").also { stage = END_DIALOGUE }
                2 -> npcl(FRIENDLY, "Not that I'm aware of, just sit back and enjoy the sunshine bwana!").also { stage = END_DIALOGUE }
                3 -> npcl(NEUTRAL, "Sorry Bwana, I'm already busy, why not go and talk to Murcaily! He's around the village somewhere.").also { stage = END_DIALOGUE }
                4 -> npcl(NEUTRAL, "Well, I think that there's some work to be done...perhaps Murcaily can help you. He usually tends to the hardwood grove to the east of Trufitus's hut.").also { stage = END_DIALOGUE }
                5 -> npcl(FRIENDLY, "It's just village life as normal around here Bwana, always something interesting to find to occupy your time if you look hard enough.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(FANELLAMAN_2523, FANELLAMAN_2525, FANELLAMAN_2524)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return FanellamanDialogue(player)
    }
}