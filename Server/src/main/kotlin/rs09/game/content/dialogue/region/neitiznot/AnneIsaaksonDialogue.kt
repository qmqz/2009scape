package rs09.game.content.dialogue.region.neitiznot

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.AMBASSADOR_FERRNOOK_4582
import org.rs09.consts.NPCs.ANNE_ISAAKSON_5512
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AnneIsaaksonDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(ASKING, "Hello visitor, how are you?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> playerl(FRIENDLY, "Better than expected. It's a lot...nicer...here than I was expecting. Everyone seems pretty happy.").also { stage++ }
            1 -> npcl(FRIENDLY, "Of course, the Burgher is strong and wise, and looks after us well.").also { stage++ }
            2 -> playerl(THINKING, "I think some of those Jatizso citizens have got the wrong idea about this place.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ANNE_ISAAKSON_5512)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AnneIsaaksonDialogue(player)
    }
}