package rs09.game.content.dialogue.region.alkharid

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SHOPKEEPER_524
import org.rs09.consts.NPCs.SHOP_ASSISTANT_525
import rs09.tools.END_DIALOGUE

/**
 * @author bushtail
 * "Henceforth, you shall be called... Craig."
 * fixed and cleaned up by qmqz
 */

@Initializable
class AlKharidShopKeeperDialogue(player: Player? = null) : DialoguePlugin(player) {

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        n(ASKING, "Can I help you at all?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> o("Yes, please. What are you selling?", "How should I use your shop?", "No, thanks.").also { stage++ }
            1 -> when(buttonId) {
                1 -> npc.openShop(player)
                2 -> p(ASKING, "How should I use your shop?").also { stage = 201 }
                3 -> p(NEUTRAL, "No, thanks.").also { stage = END_DIALOGUE }
            }
            2 -> nl(FRIENDLY, "I'm glad you ask! You can buy as many of the items stocked as you wish. You can also sell most items to the shop.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AlKharidShopKeeperDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(SHOPKEEPER_524, SHOP_ASSISTANT_525)
    }
}