package rs09.game.content.dialogue.region.apeatoll.marim

import api.isEquipped
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.Items
import org.rs09.consts.NPCs.IFABA_1436
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class IfabaDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        if(isEquipped(player, Items.MSPEAK_AMULET_4021)) {
            npcl(FacialExpression.OLD_CALM_TALK2, "Would you like to buy or sell anything?").also { stage = 10 }
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            10 -> options("Yes, please.", "No, thanks.").also { stage++ }
            11 -> when (buttonId) {
                1 -> player(FacialExpression.FRIENDLY, "Yes, please.").also { npc.openShop(player) }
                2 -> player(FacialExpression.FRIENDLY, "No, thanks.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return IfabaDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(IFABA_1436)
    }
}