package rs09.game.content.dialogue.region.keldagrim.west

import api.sendNPCDialogue
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.HAERA_2189
import org.rs09.consts.NPCs.HEGIR_2188
import rs09.tools.END_DIALOGUE
import java.util.*

/**
 * @author qmqz
 */

@Initializable
class HegirDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(ASKING, "Hello, can I ask you a question?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        val gender = if (player.isMale) "he" else "she"
        val gender2 = if (player.isMale) "him" else "her"
        val gender3 = if (player.isMale) "his" else "her"

        when(stage){
            0 -> npcl(OLD_DEFAULT, "Oh yes, young fellow? Yes, what do you want?").also { stage++ }
            1 -> sendNPCDialogue(player, HAERA_2189, "Who's that you're talking to Hegir?", OLD_ANGRY2).also { stage++ }
            2 -> npcl(OLD_CALM_TALK1, "Let me see, $gender appears to be a human of some sort.").also { stage++ }
            3 -> npcl(OLD_CALM_TALK1, "What was your name again, human?").also { stage++ }
            4 -> player(FRIENDLY, "${player.name.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }}.").also { stage++ }
            5 -> npcl(OLD_CALM_TALK1, "It's ${player.name.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }}!").also { stage++ }
            6 -> sendNPCDialogue(player, HAERA_2189, "Well ask $gender2 what $gender wants then!", OLD_ANGRY2).also { stage++ }
            7 -> npcl(OLD_CALM_TALK1, "What do you want, human?").also { stage++ }
            8 -> player(GUILTY, "Errr... I don't know... A quest, maybe?").also { stage++ }
            9 -> npcl(OLD_CALM_TALK1, "${gender.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }} says $gender's looking for $gender3 quest!").also { stage++ }
            10 -> sendNPCDialogue(player, HAERA_2189, "What makes $gender2 think $gender lost $gender3 quest in here?", OLD_ANGRY2).also { stage++ }
            11 -> npcl(OLD_CALM_TALK1, "What makes you think you lost your quest in here?").also { stage++ }
            12 -> playerl(HALF_ASKING, "No no, I mean, do you have a task of some sort for me?").also { stage++ }
            13 -> npcl(OLD_CALM_TALK1, "Now $gender says $gender wants a task!").also { stage++ }
            14 -> sendNPCDialogue(player, HAERA_2189, "Stop talking to that human and get back in here before I have a task for you!", OLD_ANGRY2).also { stage++ }
            15 -> npcl(OLD_CALM_TALK1, "No need to talk to me like that, Haera.").also { stage++ }
            16 -> sendNPCDialogue(player, HAERA_2189, "I'll talk to you in whatever way I like!", OLD_ANGRY2).also { stage++ }
            17 -> playerl(WORRIED, "I think I hear someone calling my name in the distance...").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return HegirDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(HEGIR_2188)
    }
}