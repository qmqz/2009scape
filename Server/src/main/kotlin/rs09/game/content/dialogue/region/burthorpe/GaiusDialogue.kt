package rs09.game.content.dialogue.region.burthorpe

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.GAIUS_586

/**
 * @author qmqz
 */

@Initializable
class GaiusDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(FRIENDLY, "Welcome to my two-handed sword shop.")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when(stage) {
            0 -> o("Let's trade.", "Thanks, but not today.").also { stage++ }
            1 -> when (b) {
                1 -> npc.openShop(player)
                2 -> end()
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(GAIUS_586)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return GaiusDialogue(player)
    }
}