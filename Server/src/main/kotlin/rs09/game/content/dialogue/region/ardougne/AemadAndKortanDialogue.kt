package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ANNOYED
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.AEMAD_590
import org.rs09.consts.NPCs.KORTAN_591
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AemadAndKortanDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(FRIENDLY, "Hello, you look like a bold adventurer. You've come to the right place for adventurers' equipment.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("Oh, that sounds interesting.", "How should I use your shop?", "No, sorry, I've come to the wrong place.").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> npcl(FRIENDLY, "I'm glad you ask! You can buy as many of the items stocked as you wish. You can also sell most items to the shop.").also { stage = 0 }
                3 -> npcl(ANNOYED, "Hmph. Well, perhaps next time you'll need something from me?").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(AEMAD_590, KORTAN_591)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AemadAndKortanDialogue(player)
    }
}