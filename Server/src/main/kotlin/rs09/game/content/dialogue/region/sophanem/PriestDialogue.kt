package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.PRIEST_1988
import org.rs09.consts.NPCs.PRIEST_1989
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class PriestDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hello.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(FRIENDLY, "Greetings, non-believer. May the blessings of Icthlarin rest on your tired shoulders and may they set you at ease during interesting times.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(PRIEST_1988, PRIEST_1989)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return PriestDialogue(player)
    }
}