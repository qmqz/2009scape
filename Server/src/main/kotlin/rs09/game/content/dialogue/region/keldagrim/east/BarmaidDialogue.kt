package rs09.game.content.dialogue.region.keldagrim.east

import api.addItemOrDrop
import api.amountInInventory
import api.removeItem
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable
import org.rs09.consts.Items.BEER_1917
import org.rs09.consts.Items.COINS_995
import org.rs09.consts.Items.STEW_2003
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BarmaidDialogue(player: Player? = null) : DialoguePlugin(player) {

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(OLD_DEFAULT, "Welcome to the Laughing Miner pub, human traveller.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("Who is that man walking around outside?", "I'd like a beer please.", "I'd like some food please.", "I have to go.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(FRIENDLY, "Who is that man walking around outside?").also { stage = 10 }
                2 -> player(FRIENDLY, "I'd like a beer please.").also { stage = 20 }
                3 -> player(FRIENDLY, "I'd like some food please.").also { stage = 30 }
                4 -> player(NEUTRAL, "I have to go.").also { stage = END_DIALOGUE }
            }

            10 -> npc(OLD_CALM_TALK1, "What man?").also { stage++ }
            11 -> player(FRIENDLY, "I mean the dwarf, the one with the sign.").also { stage++ }
            12 -> npcl(OLD_CALM_TALK2, "Oh, him. Yes, we employ him to advertise our pub, he's the cheapest labour we could find. We don't have a lot of money to spare you know, we pay him in beer.").also { stage++ }
            13 -> player(ASKING, "But what's wrong with him?").also { stage++ }
            14 -> npc(OLD_CALM_TALK1, "Well, he's drunk isn't he?").also { stage++ }
            15 -> npcl(OLD_CALM_TALK2, "I told you, he's cheap. He couldn't get any other work since the Red Axe fired him. Been drinking ever since.").also { stage++ }
            16 -> player(ASKING, "How did that happen?").also { stage++ }
            17 -> npcl(OLD_CALM_TALK1, "I'm not quite sure, I think he kept wearing the wrong coloured cap all the time. They don't much like it if you don't wear your red uniform into work.").also { stage++ }
            18 -> player(AMAZED, "They fired him for that??").also { stage++ }
            19 -> npcl(OLD_CALM_TALK2, "The Red Axe will fire you for just about anything if they want to.").also { stage = END_DIALOGUE }


            20 -> npc(OLD_DEFAULT, "That'll be 2 gold coins.").also { stage++ }

            21 -> options("Pay.", "Don't pay.").also { stage++ }
            22 -> when (buttonId) {
                1 -> {
                    if (amountInInventory(player, COINS_995) >= 2) {
                        player(WORRIED, "Sorry, I don't have 2 coins on me.").also { stage = END_DIALOGUE }
                    } else {
                        removeItem(player, Item(COINS_995, 2))
                        addItemOrDrop(player, BEER_1917)
                        npc(OLD_DEFAULT, "Thanks for your custom.").also { stage = END_DIALOGUE }
                    }
                }
                2 -> player(NEUTRAL, "Sorry, I changed my mind.").also { stage = END_DIALOGUE }
            }

            30 -> npc(OLD_DEFAULT, "I can make you a stew for 20 gold coins.").also { stage++ }

            31 -> options("Pay.", "Don't pay.").also { stage++ }
            32 -> when (buttonId) {
                1 -> {
                    if (amountInInventory(player, COINS_995) < 20) {
                        player(WORRIED, "Sorry, I don't have 20 coins on me.").also { stage = END_DIALOGUE }
                    } else {
                        removeItem(player, Item(COINS_995, 20))
                        addItemOrDrop(player, STEW_2003)
                        npc(OLD_DEFAULT, "Thanks for your custom.").also { stage = END_DIALOGUE }
                    }
                }
                2 -> player(NEUTRAL, "Sorry, I changed my mind.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.BARMAID_2178)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BarmaidDialogue(player)
    }
}