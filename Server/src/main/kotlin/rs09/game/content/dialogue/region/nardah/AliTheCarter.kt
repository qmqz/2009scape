package rs09.game.content.dialogue.region.nardah

import api.addItemOrDrop
import api.amountInInventory
import api.removeItem
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.game.node.item.Item
import core.plugin.Initializable
import org.rs09.consts.Items.COINS_995
import org.rs09.consts.Items.WATERSKIN4_1823
import org.rs09.consts.NPCs.ALI_THE_CARTER_3030
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AliTheCarter(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(FRIENDLY,"Water, water get your fresh water here, 1000 coins a waterskin, a bargain in these difficult times.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("Thats extortionate! Why so much?", "Ok, I'll buy some!", "I think I'll give it a miss.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(ASKING, "Thats extortionate! Why so much?").also { stage = 10 }
                2 -> {
                    if (amountInInventory(player, COINS_995) >= 1000) {
                        removeItem(player, Item(COINS_995, 1000))
                        addItemOrDrop(player, WATERSKIN4_1823, 1)
                        end()
                    } else {
                        sendDialogue("You don't have enough coins to buy that.").also { stage = END_DIALOGUE }
                    }
                }
                3 -> player(ANNOYED, "I think I'll give it a miss.").also { stage = END_DIALOGUE }
            }

            10 -> npcl(ANNOYED, "Well it's the cheapest water you'll find in this town.").also { stage = 0 }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AliTheCarter(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(ALI_THE_CARTER_3030)
    }
}