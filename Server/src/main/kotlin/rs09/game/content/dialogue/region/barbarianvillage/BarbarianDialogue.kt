package rs09.game.content.dialogue.region.barbarianvillage

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BARBARIAN_12
import org.rs09.consts.NPCs.BARBARIAN_3246
import org.rs09.consts.NPCs.BARBARIAN_3247
import org.rs09.consts.NPCs.BARBARIAN_3248
import org.rs09.consts.NPCs.BARBARIAN_3249
import org.rs09.consts.NPCs.BARBARIAN_3250
import org.rs09.consts.NPCs.BARBARIAN_3251
import org.rs09.consts.NPCs.BARBARIAN_3252
import org.rs09.consts.NPCs.BARBARIAN_3253
import org.rs09.consts.NPCs.BARBARIAN_3255
import org.rs09.consts.NPCs.BARBARIAN_3256
import org.rs09.consts.NPCs.BARBARIAN_3257
import org.rs09.consts.NPCs.BARBARIAN_3258
import org.rs09.consts.NPCs.BARBARIAN_3259
import org.rs09.consts.NPCs.BARBARIAN_3260
import org.rs09.consts.NPCs.BARBARIAN_3261
import org.rs09.consts.NPCs.BARBARIAN_3262
import org.rs09.consts.NPCs.BARBARIAN_3263
import org.rs09.consts.NPCs.BARBARIAN_5909
import rs09.tools.END_DIALOGUE

@Initializable
class BarbarianDialogue(player: Player? = null) : DialoguePlugin(player) {

    override fun open(vararg args: Any): Boolean {
        npc = args[0] as NPC
        npc(FacialExpression.HALF_GUILTY, "What do you want, outerlander?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> options("Let's fight!", "Goodbye.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(FacialExpression.HALF_GUILTY, "Let's fight!").also { stage = 20 }

                2 -> player(FacialExpression.HALF_GUILTY, "Goodbye.").also { stage = END_DIALOGUE }
            }

            20 -> npc.properties.combatPulse.attack(player)
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(BARBARIAN_12, BARBARIAN_3246, BARBARIAN_3247, BARBARIAN_3248, BARBARIAN_3249, BARBARIAN_3250,
            BARBARIAN_3251, BARBARIAN_3252, BARBARIAN_3253, BARBARIAN_3255, BARBARIAN_3256, BARBARIAN_3257, BARBARIAN_3258, BARBARIAN_3259,
            BARBARIAN_3260, BARBARIAN_3261, BARBARIAN_3262, BARBARIAN_3263, BARBARIAN_5909)
    }

    override fun newInstance(player: Player): DialoguePlugin {
        return BarbarianDialogue(player)
    }

}

