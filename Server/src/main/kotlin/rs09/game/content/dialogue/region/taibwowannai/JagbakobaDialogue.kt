package rs09.game.content.dialogue.region.taibwowannai

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.JAGBAKOBA_2526
import org.rs09.consts.NPCs.JAGBAKOBA_2527
import org.rs09.consts.NPCs.JAGBAKOBA_2528
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class JagbakobaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> when (buttonId) {
                1 -> player(ASKING, "What do you do here?").also { stage = 10 }
                2 -> player(ASKING, "Is there anything interesting to do around here?").also { stage = 20 }
                3 -> player(FRIENDLY, "Ok, thanks.").also { stage = END_DIALOGUE }
            }
            1 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }

            10 -> npcl(FRIENDLY, "Why, it should be clear to even the most ignorant of people that I am a fearless jungle hunter...out of my way you sanitised civilian!").also { stage = END_DIALOGUE }

            20 -> npcl(NEUTRAL, "There's always something interesting to occupy your time if you look hard enough, Bwana. Perhaps Murcaily can help you.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(JAGBAKOBA_2526, JAGBAKOBA_2527, JAGBAKOBA_2528)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return JagbakobaDialogue(player)
    }
}