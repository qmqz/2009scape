package rs09.game.content.dialogue.region.burthorpe.castle

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.EOHRIC_1080
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class EohricDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        n(ASKING, "Hello. Can I help?")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when(stage) {
            0 -> o("What is this place?", "That's quite an outfit.").also { stage++ }
            1 -> when (b) {
                1 -> nl(FRIENDLY, "This is Burthorpe Castle, home to His Royal Highness Prince Anlaf, heir to the throne of Asgarnia.").also { stage = 10 }
                2 -> nl(HAPPY, "Why, thank you. I designed it myself. I've always found purple such a cheerful colour!").also { stage = 0 }
            }

            10 -> n(FRIENDLY, "No doubt you're impressed.").also { stage++ }

            11 -> o("Where is the prince?", "Goodbye.").also { stage++ }
            12 -> when (b) {
                1 -> nl(SUSPICIOUS, "I cannot disclose the prince's exact whereabouts for fear of compromising his personal safety.").also { stage = 20 }
                2 -> end()
            }

            20 -> nl(NEUTRAL, "But rest assured that he is working tirelessly to maintain the safety and wellbeing of Burthorpe's people.").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(EOHRIC_1080)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return EohricDialogue(player)
    }
}