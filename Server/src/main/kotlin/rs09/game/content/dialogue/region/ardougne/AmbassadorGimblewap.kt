package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.AMBASSADOR_GIMBLEWAP_4580
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AmbassadorGimblewap(player: Player? = null) : DialoguePlugin(player) {


    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(OLD_DISTRESSED, "I can't believe that I've been made to wait so long.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> player(ASKING, "What are you waiting for?").also { stage++ }
            1 -> npcl(OLD_DISTRESSED2, "I have been waiting for an audience with the King for days!").also { stage++ }
            2 -> player(HALF_THINKING, "Well, he's clearly a busy man.").also { stage++ }
            3 -> npcl(OLD_DISTRESSED, "Bah! I think he's just trying to insult the gnome nation, by making me wait. The cheek of these humans. Just because they're vertically challenged they think they can ignore us.").also { stage++ }
            4 -> player(FRIENDLY, "I'm not ignoring you.").also { stage++ }
            5 -> npcl(OLD_CALM_TALK1, "Hmm. I should be thankful for small mercies, I suppose.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(AMBASSADOR_GIMBLEWAP_4580)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AmbassadorGimblewap(player)
    }
}