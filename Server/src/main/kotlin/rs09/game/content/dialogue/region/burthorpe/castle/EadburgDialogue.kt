package rs09.game.content.dialogue.region.burthorpe.castle

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.EADBURG_1072
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class EadburgDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        p(ASKING, "Hello there. What's in the pot?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> n(FRIENDLY, "The stew for the servants' main meal.").also { stage++ }
            1 -> p(ASKING, "Can I have some?").also { stage++ }
            2 -> nl(NEUTRAL, "Well not unless you become a servant. There's a war on, you know, so food is scarce.").also { stage++ }
            3 -> pl(HALF_GUILTY, "Good point. I should probably leave you to it.").also { stage++ }
            4 -> n(NEUTRAL, "Bye then.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(EADBURG_1072)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return EadburgDialogue(player)
    }
}