package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.SAD
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.PRIEST_358
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class PriestDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hello there.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(SAD, "I wish there was more I could do for these people.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(PRIEST_358)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return PriestDialogue(player)
    }
}