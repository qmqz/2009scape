package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BAKER_571
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BakerDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        val gender = if (player.isMale) "monsieur" else "madame"
        npc = args[0] as NPC
        npcl(ASKING, "Good day, $gender. Would you like ze nice freshly-baked bread? Or perhaps a nice piece of cake?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("Let's see what you have.", "No, thank you.").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(FRIENDLY, "No, thank you.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(BAKER_571)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BakerDialogue(player)
    }
}