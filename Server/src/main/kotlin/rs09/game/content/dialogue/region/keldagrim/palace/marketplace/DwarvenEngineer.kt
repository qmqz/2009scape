package rs09.game.content.dialogue.region.keldagrim.palace.marketplace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.DWARVEN_ENGINEER_1840
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class DwarvenEngineer(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(OLD_SAD,"I wish you'd just leave me alone.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> p(ASKING, "Why, what's wrong?").also { stage++ }
            1 -> nl(OLD_SAD, "I was a great engineer once. Still am actually.").also { stage++ }
            2 -> nl(OLD_SAD, "They just don't let me do the work I do best anymore.").also { stage++ }
            3 -> p(ASKING, "What do you do then?").also { stage++ }
            4 -> nl(OLD_SAD, "I'm an assistant in this shop. I get all the lousy jobs too.").also { stage++ }
            5 -> p(GUILTY, "Well, I'm sorry to hear that.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return DwarvenEngineer(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(DWARVEN_ENGINEER_1840)
    }
}