package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class RokuhDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(FRIENDLY,"Come one, come all, buy my amazing choc ice invention here! Chocolate on the outside. Iced cream on the inside. Oh I also have some chocolate left over from making choc ices which I'm selling too.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("Cool, I'd like to buy some.", "No thanks, I'm not interested.", "How do you stop your icy snacks melting?").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(FRIENDLY, "No thanks, I'm not interested.").also { stage = END_DIALOGUE }
                3 -> playerl(ASKING, "How do you stop your icy snacks melting? The middle of the desert is the last place I'd expect to see ice.").also { stage = 10 }
            }

            10 -> npcl(FRIENDLY, "It's quite a surprise, isn't it, my friend? I actually have this special magic box of ice, which I bought for a princely sum from a strange man while adventuring in lands far away.").also { stage++ }
            11 -> npcl(FRIENDLY, "He said it is imbued with some sort of powerful ice magic which keeps it cold all the time. I had never seen any ice magic before. It's clearly very rare and powerful,").also { stage++ }
            12 -> npcl(FRIENDLY, "so I felt I had to buy it.").also { stage = 0 }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return RokuhDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.ROKUH_3045)
    }
}