package rs09.game.content.dialogue.region.canifis

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BARKER_1039
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BarkerDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(FRIENDLY, "You are looking for clothes, yes? You look at my products! I have very many nice clothes, yes?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            1 -> options("Yes, please.", "No thanks.").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes, please.").also { stage = 10 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 20 }
            }

            10 -> npc.openShop(player)

            20 -> npcl(ASKING, "Unfortunate for you, yes? Many bargains, won't find elsewhere!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(BARKER_1039)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BarkerDialogue(player)
    }
}