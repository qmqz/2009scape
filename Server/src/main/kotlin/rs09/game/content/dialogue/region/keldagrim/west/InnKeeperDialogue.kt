package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.INN_KEEPER_2177
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class InnKeeperDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY,"Hello.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> npcl(OLD_DEFAULT, "Welcome to the King's Axe inn! What can I help you with?").also { stage++ }
            1 -> player(ASKING, "Can I have some beer please?").also { stage++ }
            2 -> npcl(OLD_DEFAULT, "Go to the bar downstairs. I only deal with residents.").also { stage++ }
            3 -> player(THINKING, "Residents? People live here?").also { stage++ }
            4 -> npcl(OLD_DEFAULT, "No, just guests that stay the night.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return InnKeeperDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(INN_KEEPER_2177)
    }
}