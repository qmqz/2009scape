package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SANTIRI_2152
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SantiriDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(OLD_CALM_TALK1, "Welcome, human, to the Quality Weapons Shop! Can I interest you in a purchase?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> o("Yes, I'm looking for some weapons.", "No thanks").also { stage++ }
            1 -> when (buttonId) {
                1 -> p(FRIENDLY, "Yes, I'm looking for some weapons.").also { npc.openShop(player) }
                2 -> p(NEUTRAL, "No thanks.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SANTIRI_2152)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SantiriDialogue(player)
    }
}