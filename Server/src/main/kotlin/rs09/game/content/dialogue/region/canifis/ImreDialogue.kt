package rs09.game.content.dialogue.region.canifis

import api.isEquipped
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.Items.RING_OF_CHAROS_4202
import org.rs09.consts.NPCs.IMRE_6027
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class ImreDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        if (!isEquipped(player, RING_OF_CHAROS_4202)) {
            when ((1..10).random()) {
                1 -> npcl(ANNOYED, "If I were as ugly as you I would not dare to show my face in public!").also { stage = END_DIALOGUE }
                2 -> npc(ANNOYED, "Out of my way, punk.").also { stage = END_DIALOGUE }
                3 -> npc(THINKING, "Hmm... you smell strange...").also { stage = 10 }
                4 -> npc(ANNOYED, "Leave me alone.").also { stage = END_DIALOGUE }
                5 -> npcl(ANNOYED, "Don't talk to me again if you value your life!").also { stage = END_DIALOGUE }
                6 -> npc(ANNOYED, "Get lost!").also { stage = END_DIALOGUE }
                7 -> npcl(ANNOYED, "I don't have anything to give you so leave me alone, mendicant.").also { stage = END_DIALOGUE }
                8 -> npc(ANNOYED, "Have you no manners?").also { stage = END_DIALOGUE }
                9 -> npcl(ANNOYED, "I don't have time for this right now.").also { stage = END_DIALOGUE }
                10 -> npcl(ANNOYED, "I have no interest in talking to a pathetic meat bag like yourself.").also { stage = END_DIALOGUE }
            }
        } else {
            when ((1..10).random()) {
                1 -> npc(ANNOYED, "Down with rutabagas!").also { stage = END_DIALOGUE }
                2 -> npcl(HAPPY, "I may be a werewolf, but I do love the smell of frying onions.").also { stage = END_DIALOGUE }
                3 -> npcl(FRIENDLY, "Onion skins very thin, mild winter coming in. Onion skins very tough, coming winter very rough.").also { stage = END_DIALOGUE }
                4 -> npcl(THINKING, "Did you know the onion is of genus Allium and species cepa?").also { stage = END_DIALOGUE }
                5 -> npcl(FRIENDLY, "When you cut onions they release a gas that makes you cry.").also { stage = END_DIALOGUE }
                6 -> npc(HALF_THINKING, "Maybe I'll start an Onion Guild.").also { stage = END_DIALOGUE }
                7 -> npcl(FRIENDLY, "People are like onions. They have lots of layers and sometimes smell quite pungent.").also { stage = END_DIALOGUE }
                8 -> npc(LAUGH, "If you hear an onion ring, answer it!").also { stage = END_DIALOGUE }
                9 -> npc(THINKING, "Life is like an onion...well, sort of.").also { stage = END_DIALOGUE }
                10 -> npc(ANNOYED, "I might try my hand at farming onions some time.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(IMRE_6027)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return ImreDialogue(player)
    }
}