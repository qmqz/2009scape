package rs09.game.content.dialogue.region.edgeville

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BROTHER_ALTHRIC_2588
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BrotherAlthric(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Very nice rosebushes you have here.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(FRIENDLY, "Yes, it has taken me many long hours in this garden to bring them to this state of near-perfection.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(BROTHER_ALTHRIC_2588)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BrotherAlthric(player)
    }
}