package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.RAETUL_1982
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class RaetulDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hello.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(ASKING, "What can I do for you?").also { stage++ }
            1 -> options("What do you have for sale?", "Nothing.").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(ASKING, "What do you have for sale?").also { stage = 10 }
                2 -> player(FRIENDLY, "Nothing.").also { stage = 20 }
            }

            10 -> npcl(FRIENDLY, "All sorts of things. Why don't you have a look?").also { stage++ }
            11 -> npc.openShop(player)

            20 -> npc(SUSPICIOUS, "Right...").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(RAETUL_1982)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return RaetulDialogue(player)
    }
}