package rs09.game.content.dialogue.region.canifis

import api.isEquipped
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.Items.RING_OF_CHAROS_4202
import org.rs09.consts.NPCs.ALEXIS_6036
import org.rs09.consts.NPCs.BORIS_6026
import org.rs09.consts.NPCs.EDUARD_6031
import org.rs09.consts.NPCs.GALINA_6038
import org.rs09.consts.NPCs.GEORGY_6033
import org.rs09.consts.NPCs.IRINA_6035
import org.rs09.consts.NPCs.JOSEPH_6029
import org.rs09.consts.NPCs.KSENIA_6040
import org.rs09.consts.NPCs.LEV_6032
import org.rs09.consts.NPCs.LILIYA_6045
import org.rs09.consts.NPCs.MILLA_6037
import org.rs09.consts.NPCs.NIKITA_6042
import org.rs09.consts.NPCs.NIKOLAI_6030
import org.rs09.consts.NPCs.SOFIYA_6039
import org.rs09.consts.NPCs.SVETLANA_6034
import org.rs09.consts.NPCs.VERA_6043
import org.rs09.consts.NPCs.YADVIGA_6041
import org.rs09.consts.NPCs.YURI_6028
import org.rs09.consts.NPCs.ZOJA_6044
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class CanifisWerewolvesDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        if (!isEquipped(player, RING_OF_CHAROS_4202)) {
            when ((1..10).random()) {
                1 -> npcl(ANNOYED, "If I were as ugly as you I would not dare to show my face in public!").also { stage = END_DIALOGUE }
                2 -> npc(ANNOYED, "Out of my way, punk.").also { stage = END_DIALOGUE }
                3 -> npc(THINKING, "Hmm... you smell strange...").also { stage = 10 }
                4 -> npc(ANNOYED, "Leave me alone.").also { stage = END_DIALOGUE }
                5 -> npcl(ANNOYED, "Don't talk to me again if you value your life!").also { stage = END_DIALOGUE }
                6 -> npc(ANNOYED, "Get lost!").also { stage = END_DIALOGUE }
                7 -> npcl(ANNOYED, "I don't have anything to give you so leave me alone, mendicant.").also { stage = END_DIALOGUE }
                8 -> npc(ANNOYED, "Have you no manners?").also { stage = END_DIALOGUE }
                9 -> npcl(ANNOYED, "I don't have time for this right now.").also { stage = END_DIALOGUE }
                10 -> npcl(ANNOYED, "I have no interest in talking to a pathetic meat bag like yourself.").also { stage = END_DIALOGUE }
            }
        } else {
            when ((1..10).random()) {
                1 -> npc(FRIENDLY, "I bet you have wonderful paws.").also { stage = END_DIALOGUE }
                2 -> npcl(FRIENDLY, "A very miserable day, altogether... enjoy it while it lasts.").also { stage = END_DIALOGUE }
                3 -> npcl(FRIENDLY, "If you catch anyone promise me you'll share.").also { stage = END_DIALOGUE }
                4 -> npc(FRIENDLY, "I haven't smelt you around here before...").also { stage = END_DIALOGUE }
                5 -> npc(FRIENDLY, "You smell familiar...").also { stage = END_DIALOGUE }
                6 -> npcl(FRIENDLY, "Seen any humans around here? I'm v-e-r-y hungry.").also { stage = END_DIALOGUE }
                7 -> npcl(FRIENDLY, "You look to me like someone with a healthy taste for blood.").also { stage = END_DIALOGUE }
                8 -> npc(FRIENDLY, "Good day to you, my friend.").also { stage = END_DIALOGUE }
                9 -> npcl(FRIENDLY, "Fancy going up to the castle for a bit of a snack?").also { stage = END_DIALOGUE }
                10 -> npcl(FRIENDLY, "Give me a moment, I have a bit of someone stuck in my teeth...").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            10 -> player(WORRIED, "Strange how?").also { stage++ }
            11 -> npc(ANNOYED, "Like a human!").also { stage++ }
            12 -> player(WORRIED, "Oh! Er... I just ate one is why!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ZOJA_6044, SVETLANA_6034, VERA_6043, YADVIGA_6041, YURI_6028, ALEXIS_6036, BORIS_6026, EDUARD_6031, GALINA_6038, GEORGY_6033, IRINA_6035,
            JOSEPH_6029, KSENIA_6040, LEV_6032, LILIYA_6045, MILLA_6037, NIKITA_6042, NIKOLAI_6030, SOFIYA_6039)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return CanifisWerewolvesDialogue(player)
    }
}