package rs09.game.content.dialogue.region.apeatoll.marim

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ABERAB_1432
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AberabDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(FacialExpression.OLD_ANGRY1,"Grr ... Get out of my way...").also { stage = END_DIALOGUE }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AberabDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(ABERAB_1432)
    }
}