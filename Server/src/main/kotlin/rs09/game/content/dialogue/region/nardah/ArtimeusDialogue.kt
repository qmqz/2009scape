package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ARTIMEUS_5109
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class ArtimeusDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(FRIENDLY,"Greetings, friend; my business here deals with Hunter related items. Is there anything in which I can interest you?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("What kinds of items do you stock?", "I'm not in the market for Hunter equipment right now, thanks.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(ASKING, "What kinds of items do you stock?").also { stage = 10 }
                2 -> playerl(FRIENDLY, "I'm not in the market for Hunter equipment right now, thanks.").also { stage = 20 }

                3 -> player(FRIENDLY, "I think I'll give it a miss.").also { stage = END_DIALOGUE }
            }

            10 -> npcl(FRIENDLY, "Take a look for yourself.").also { stage++ }
            11 -> npc.openShop(player)

            20 -> npc(FRIENDLY, "Maybe another time, then.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return ArtimeusDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(ARTIMEUS_5109)
    }
}