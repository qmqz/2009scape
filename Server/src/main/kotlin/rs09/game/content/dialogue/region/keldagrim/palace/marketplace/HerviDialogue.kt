package rs09.game.content.dialogue.region.keldagrim.palace.marketplace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.OLD_CALM_TALK2
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.HERVI_2157
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class HerviDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(OLD_CALM_TALK2,"Greetings, human... can I interest you in some fine gems?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> o("Show me the wares!", "No thanks.").also { stage++ }

            1 -> when(buttonId) {
                1 -> p(FRIENDLY, "Show me the wares!").also { npc.openShop(player) }
                2 -> p(FRIENDLY, "No thanks.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return HerviDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(HERVI_2157)
    }
}