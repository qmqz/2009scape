package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ANNOYED
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class DoormanDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(ANNOYED, "Nobody may enter or leave Sophanem for any reason, so says the new High Priest of Icthlarin.").also { stage = END_DIALOGUE }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.DOORMAN_6769)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return DoormanDialogue(player)
    }
}