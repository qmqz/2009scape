package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.MYNDILL_2197
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class MyndillDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        when ((1..5).random()) {
            1 -> npc(OLD_DEFAULT, "And how can I help you?")
            2 -> npc(OLD_DEFAULT, "Hello there, human")
            3 -> npc(OLD_DEFAULT, "How are you doing?")
            4 -> npcl(OLD_DEFAULT, "Another beautiful day in Keldagrim, I should say!")
            5 -> npc(OLD_DEFAULT, "Greetings to you.")
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> options("Do you have any quests?", "See you later!").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(ASKING, "Do you have any quests?").also { stage = 10 }
                2 -> player(FRIENDLY, "See you later!").also { stage = 20 }
            }

            10 -> npcl(OLD_CALM_TALK1, "Let me think for a moment... No, I have nothing that I could possibly want doing. Try the shops of the market instead.").also { stage = END_DIALOGUE }
            20 -> {
                when ((1..2).random()) {
                    1 -> npc(OLD_CALM_TALK2, "And you!").also { stage = END_DIALOGUE }
                    2 -> npc(OLD_CALM_TALK2, "Perhaps!").also { stage = END_DIALOGUE }
                }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(MYNDILL_2197)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return MyndillDialogue(player)
    }
}