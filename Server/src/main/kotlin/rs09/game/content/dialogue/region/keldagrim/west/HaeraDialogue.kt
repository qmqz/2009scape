package rs09.game.content.dialogue.region.keldagrim.west

import api.sendNPCDialogue
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.HAERA_2189
import org.rs09.consts.NPCs.HEGIR_2188
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class HaeraDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(OLD_DISTRESSED,"What are you doing in our home?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        val gender = if (player.isMale) "he" else "she"
        val gender2 = if (player.isMale) "him" else "her"
        val gender3 = if (player.isMale) "himself" else "herself"

        when(stage){
            0 -> player(ASKING, "I just came to ask you a question.").also { stage++ }
            1 -> npcl(OLD_ANGRY1, "Hegir, why did you let this human into our home?").also { stage++ }
            2 -> sendNPCDialogue(player, HEGIR_2188, "I didn't let $gender2 in, $gender let $gender3 in!", OLD_DISTRESSED).also { stage++ }
            3 -> npcl(OLD_ANGRY1, "Now what do you want, human? Speak up, speak up!").also { stage++ }
            4 -> playerl(GUILTY, "I ehm, just wanted some information.").also { stage++ }
            5 -> npcl(OLD_ANGRY1, "Well go ask someone else instead of barging into people's homes!").also { stage++ }
            6 -> sendNPCDialogue(player, HEGIR_2188, "There's no need to be rude to the human, Haera dear.", OLD_CALM_TALK1).also { stage++ }
            7 -> npcl(OLD_ANGRY1, "Now don't you dear me. I'll do whatever I please.").also { stage++ }
            8 -> player(WORRIED, "I think this is my cue to leave...").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return HaeraDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(HAERA_2189)
    }
}