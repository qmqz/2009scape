package rs09.game.content.dialogue.region.icemountain

import api.hasStartedQuest
import api.questStage
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ORACLE_746
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class OracleDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        if (hasStartedQuest(player, "Dragon Slayer")) {
            when (questStage(player, "Dragon Slayer")) {
                20 -> pl(FRIENDLY, "I seek a piece of the map to the island of Crandor.")
                else -> pl(ASKING, "Can you impart your wise knowledge to me, O Oracle?").also { stage = 10 }
            }
        } else {
            pl(ASKING, "Can you impart your wise knowledge to me, O Oracle?").also { stage = 10 }
        }
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when (stage) {
            0 -> nl(FRIENDLY, "The map's behind a door below, but entering is rather tough. This is that you need to know: You must use the following stuff.").also { stage++ }
            1 -> nl(FRIENDLY, "First, a drink used by a mage. Next, some worm string, changed to sheet. Then, a small crustacean cage. Last, a bowl that's not seen heat.").also { stage = END_DIALOGUE }

            10 -> {
                when ((1..30).random()) {
                    1 -> n(FRIENDLY, "").also { stage = END_DIALOGUE }
                    2 -> nl(FRIENDLY, "The light at the end of the tunnel is the demon-infested lava pit.").also { stage = END_DIALOGUE }
                    3 -> nl(FRIENDLY, "Beware the cabbage: it is both green AND leafy.").also { stage = END_DIALOGUE }
                    4 -> nl(FRIENDLY, "They say that ham does not mix well with other kinds of meat.").also { stage = END_DIALOGUE }
                    5 -> n(FRIENDLY, "A woodchuck does not chuck wood.").also { stage = END_DIALOGUE }
                    6 -> n(FRIENDLY, "No. I'm not in the mood.").also { stage = END_DIALOGUE }
                    7 -> n(FRIENDLY, "It's not you; it's me.").also { stage = END_DIALOGUE }
                    8 -> n(FRIENDLY, "Too many cooks spoil the anchovy pizza.").also { stage = END_DIALOGUE }
                    9 -> nl(FRIENDLY, "The God Wars are over...as long as the thing they were fighting over remains hidden.").also { stage = END_DIALOGUE }
                    10 -> nl(FRIENDLY, "He who uses the power of custard mixes it with his tears.").also { stage = END_DIALOGUE }
                    11 -> n(FRIENDLY, "Nothing like a tasty fish.").also { stage = END_DIALOGUE }
                    12 -> nl(FRIENDLY, "Don't judge a book by its cover - judge it on its' grammar and, punctuation.").also { stage = END_DIALOGUE }
                    13 -> n(FRIENDLY, "Yes, I can. But I'm not going to.").also { stage = END_DIALOGUE }
                    14 -> n(FRIENDLY, "A bird in the hand can make a tasty snack.").also { stage = END_DIALOGUE }
                    15 -> n(FRIENDLY, "Pies... they're great, aren't they?").also { stage = END_DIALOGUE }
                    16 -> n(FRIENDLY, "Who guards the guardsmen?").also { stage = END_DIALOGUE }
                    17 -> nl(FRIENDLY, "The great snake of Guthix guards more than she knows.").also { stage = END_DIALOGUE }
                    18 -> nl(FRIENDLY, "Do not fear the dragons... fear their kin.").also { stage = END_DIALOGUE }
                    19 -> nl(FRIENDLY, "Everyone you know will one day be dead.").also { stage = END_DIALOGUE }
                    20 -> nl(FRIENDLY, "If a tree falls in the forest and no one is around, then nobody gets Woodcutting xp.").also { stage = END_DIALOGUE }
                    21 -> nl(FRIENDLY, "Sometimes you get lucky, sometimes you don't.").also { stage = END_DIALOGUE }
                    22 -> n(FRIENDLY, "The chicken came before the egg.").also { stage = END_DIALOGUE }
                    23 -> n(FRIENDLY, "Is it time to wake up? I am not sure...").also { stage = END_DIALOGUE }
                    24 -> n(FRIENDLY, "When in Asgarnia, do as the Asgarnians do.").also { stage = END_DIALOGUE }
                    25 -> nl(FRIENDLY, "An answer is unimportant; it is the question that matters.").also { stage = END_DIALOGUE }
                    26 -> nl(FRIENDLY, "The goblins will never make up their minds on their own.").also { stage = END_DIALOGUE }
                    27 -> n(FRIENDLY, "Jas left a stone behind.").also { stage = END_DIALOGUE }
                    28 -> n(FRIENDLY, "Many secrets are buried under this land.").also { stage = END_DIALOGUE }
                    29 -> n(FRIENDLY, "There are no crisps at the party.").also { stage = END_DIALOGUE }
                    30 -> n(FRIENDLY, "Help wanted? Enquire within.").also { stage = END_DIALOGUE }
                }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ORACLE_746)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return OracleDialogue(player)
    }
}