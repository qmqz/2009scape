package rs09.game.content.dialogue.region.keldagrim.palace.marketplace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.OLD_CALM_TALK2
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.GUNSLIK_2154
import org.rs09.consts.NPCs.GUNSLIK_2424
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class GunslikDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        nl(OLD_CALM_TALK2,"What can I interest you in? We have something of everything here!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> o("Oh good!", "Nothing, thanks.").also { stage++ }

            1 -> when(buttonId) {
                1 -> p(FRIENDLY, "Oh good!").also { npc.openShop(player) }
                2 -> p(FRIENDLY, "Nothing, thanks.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return GunslikDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(GUNSLIK_2154, GUNSLIK_2424)
    }
}