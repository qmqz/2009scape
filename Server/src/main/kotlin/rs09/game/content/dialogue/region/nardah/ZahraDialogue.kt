package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.NEUTRAL
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ZAHRA_3036
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class ZahraDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(FacialExpression.SAD,"Hello.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> player(ASKING, "You don't look too happy.").also { stage++ }
            1 -> npcl(NEUTRAL, "True. We've not fallen on the best of times here.").also { stage++ }
            2 -> player(ASKING, "Any way that I can help?").also { stage++ }
            3 -> npcl(NEUTRAL, "Possibly. I'd go talk to Awusah the Mayor of Nardah. He's in the big house on the east side of the town square.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return ZahraDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(ZAHRA_3036)
    }
}