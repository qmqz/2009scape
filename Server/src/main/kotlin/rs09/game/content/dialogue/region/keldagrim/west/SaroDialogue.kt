package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SARO_2153
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SaroDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(OLD_CALM_TALK1, "Welcome to my store, human! Are you interested in buying anything?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("Yes, I'm looking for some armour.", "No thanks").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes, I'm looking for some armour.").also { stage = 10 }
                2 -> player(NEUTRAL, "No thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npc.openShop(player)
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SARO_2153)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SaroDialogue(player)
    }
}