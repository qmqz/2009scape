package rs09.game.content.dialogue.region.ardougne

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.RICHARD_2306
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class RichardDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(ASKING, "Hello. How can I help you?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("What are you selling?", "Can you give me any farming advice?", "I'm okay, thank you.").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(FRIENDLY, "Can you give me any farming advice?").also { stage = 10 }
                3 -> player(FRIENDLY, "I'm okay, thank you.").also { stage = END_DIALOGUE }
            }

            10 -> npc(ANNOYED, "Yes - ask a gardener.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(RICHARD_2306)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return RichardDialogue(player)
    }
}