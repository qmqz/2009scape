package rs09.game.content.dialogue.region.burthorpe

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BREOCA_1084
import org.rs09.consts.NPCs.CEOLBURG_1089
import org.rs09.consts.NPCs.HYGD_1088
import org.rs09.consts.NPCs.OCGA_1085
import org.rs09.consts.NPCs.PENDA_1087
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BurthorpeCitizenDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
            when ((1..5).random()) {
                1 -> npc(FRIENDLY, "Hello there!").also { stage = END_DIALOGUE }
                2 -> npc(FRIENDLY, "Good day!").also { stage = END_DIALOGUE }
                3 -> npc(FRIENDLY, "Lovely day, isn't it?").also { stage = END_DIALOGUE }
                4 -> npc(FRIENDLY, "Oh, hello.").also { stage = END_DIALOGUE }
                5 -> npc(FRIENDLY, "Good to see you!").also { stage = END_DIALOGUE }
            }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(OCGA_1085, PENDA_1087, BREOCA_1084, CEOLBURG_1089, HYGD_1088)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BurthorpeCitizenDialogue(player)
    }
}