package rs09.game.content.dialogue.region.draynor

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.OLIVIA_2233
import org.rs09.consts.NPCs.OLIVIA_2572
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class OliviaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        o("Would you like to trade?", "Where do I get higher-level seeds?")
        return true
    }

    override fun handle(interfaceId: Int, b: Int): Boolean {
        when (stage) {
            0 -> when (b) {
                1 -> n(FRIENDLY, "Certainly.").also { npc.openShop(player) }
                2 -> p(ASKING, "Where do I get higher-level seeds?").also { stage = 10 }
            }

            10 -> nl(FRIENDLY, "Master farmers usually carry a few higher-level seeds around with them, although I don't know if they'd want to part with them for any price, to be honest.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(OLIVIA_2233, OLIVIA_2572)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return OliviaDialogue(player)
    }
}