package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.GHASLOR_THE_ELDER_3029
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class GhaslorDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        val gender = if (player.isMale) "man" else "lady"
        npc = args[0] as NPC
        npc(FRIENDLY,"Good day to you young ${gender}.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> playerl(FRIENDLY, "Hello, I'm an adventurer in search of err.. adventure.").also { stage++ }
            1 -> npcl(FRIENDLY, "Well I'm Ghaslor, the oldest person in this here town and keeper of some of the lores and histories of our village. If you're new around here I'd go talk to the mayor.").also { stage++ }
            2 -> player(FRIENDLY, "Thanks, I might go and look him up then.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return GhaslorDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(GHASLOR_THE_ELDER_3029)
    }
}