package rs09.game.content.dialogue.region.keldagrim.palace.marketplace

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.OLD_DEFAULT
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.NOLAR_2158
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class NolarDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npcl(OLD_DEFAULT,"I have a wide variety of crafting tools on offer, care to take a look?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("Yes please!", "No thanks").also { stage++ }

            1 -> when(buttonId) {
                1 -> player(FRIENDLY, "Yes please!").also { stage = 10 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npc.openShop(player)
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return NolarDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NOLAR_2158)
    }
}