package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SLAVE_1978
import org.rs09.consts.NPCs.SLAVE_1979
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SlaveDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hi!")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> sendDialogue("The slave gives you a murderous look, but is too busy to respond to you.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SLAVE_1978, SLAVE_1979)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SlaveDialogue(player)
    }
}