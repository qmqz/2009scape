package rs09.game.content.dialogue.region.taibwowannai

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.LAYLEEN_2511
import org.rs09.consts.NPCs.LAYLEEN_2512
import org.rs09.consts.NPCs.LAYLEEN_2513
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class LayleenDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> when (buttonId) {
                1 -> player(ASKING, "What do you do here?").also { stage = 10 }
                2 -> player(ASKING, "Is there anything interesting to do around here?").also { stage = 20 }
                3 -> player(FRIENDLY, "Ok, thanks.").also { stage = END_DIALOGUE }
            }
            1 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }

            10 -> npcl(FRIENDLY, "Oh, I just walk around a bit...I have to help my Mum and Dad a lot though because I'm still only young. I'd love to visit the mainland one day though and get out of this boring village.").also { stage = 1 }

            20 -> npcl(NEUTRAL, "Not that I'm aware of, just sit back and enjoy the sunshine bwana!").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(LAYLEEN_2511, LAYLEEN_2512, LAYLEEN_2513)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return LayleenDialogue(player)
    }
}