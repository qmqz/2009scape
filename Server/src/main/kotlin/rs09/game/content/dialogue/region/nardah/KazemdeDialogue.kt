package rs09.game.content.dialogue.region.nardah

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.ASKING
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.KAZEMDE_3039

/**
 * @author qmqz
 */

@Initializable
class KazemdeDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(ASKING,"Can I help you at all?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> options("Yes please. What are you selling?", "No thanks.").also { stage++ }
            1 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> end()
            }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return KazemdeDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(KAZEMDE_3039)
    }
}