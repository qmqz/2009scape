package rs09.game.content.dialogue.region.taibwowannai

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.MAMMA_BUFETTA_2508
import org.rs09.consts.NPCs.MAMMA_BUFETTA_2509
import org.rs09.consts.NPCs.MAMMA_BUFETTA_2510
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class MammaBufettaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> when (buttonId) {
                1 -> player(ASKING, "What do you do here?").also { stage = 10 }
                2 -> player(ASKING, "Is there anything interesting to do around here?").also { stage = 200 }
                3 -> player(FRIENDLY, "Ok, thanks.").also { stage = END_DIALOGUE }
            }
            1 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }

            10 -> npcl(FRIENDLY, "I's been cooking up some roasted spiders....I can show you how to make 'em if you like?").also { stage++ }

            11 -> options("Errrr..yuck, roasted spiders, that sounds gross!", "Hmm, I guess it could be considered a delicacy, so yes please?", "I'd like to ask another question.", "Sorry, I have to go.").also { stage++ }
            12 -> when (buttonId) {
                1 -> playerl(DISGUSTED, "Errrr..yuck, roasted spiders, that sounds gross!").also { stage = 100 }
                2 -> playerl(THINKING, "Hmm, I guess it could be considered a delicacy, so yes please?").also { stage = 110 }
                3 -> options("What do you do here?", "Is there anything interesting to do here?", "Ok, thanks.").also { stage = 0 }
                4 -> player(NEUTRAL, "Sorry, I have to go.").also { stage = END_DIALOGUE }
            }

            100 -> npcl(LAUGH, "Well, you can suit yourself my friend, but I have to tell you they're delicious!").also { stage++ }
            101 -> npcl(LAUGH, "Mmmm, mmm, mmmmmm!").also { stage = END_DIALOGUE }

            110 -> npcl(FRIENDLY, "Ok my friend, you simply need to get a suitable stick to skewer the spider carcass on and then roast it on a fire!").also { stage++ }
            111 -> player(ASKING, "What sort of stick and where would I get one?").also { stage++ }
            112 -> npcl(FRIENDLY, "Well, I prefer to use a skewer stick, you can make one from a thatching spar using a machete. Or, if you're stuck, you could always use an arrow shaft, though they tend to be useless once you've finished eating the").also { stage++ }
            113 -> npc(FRIENDLY, "spider.").also { stage++ }
            114 -> player(FRIENDLY, "Ok, thanks!").also { stage = END_DIALOGUE }



            200 -> when ((1..5).random()) {
                1 -> npcl(ANNOYED, "Bwana, if anyone else asks me that question today, I'll explode! Go and ask someone else!").also { stage = END_DIALOGUE }
                2 -> npcl(FRIENDLY, "Not that I'm aware of, just sit back and enjoy the sunshine bwana!").also { stage = END_DIALOGUE }
                3 -> npcl(NEUTRAL, "Sorry Bwana, I'm already busy, why not go and talk to Murcaily! He's around the village somewhere.").also { stage = END_DIALOGUE }
                4 -> npcl(NEUTRAL, "Well, I think that there's some work to be done...perhaps Murcaily can help you. He usually tends to the hardwood grove to the east of Trufitus's hut.").also { stage = END_DIALOGUE }
                5 -> npcl(FRIENDLY, "It's just village life as normal around here Bwana, always something interesting to find to occupy your time if you look hard enough.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(MAMMA_BUFETTA_2508, MAMMA_BUFETTA_2509, MAMMA_BUFETTA_2510)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return MammaBufettaDialogue(player)
    }
}