package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.NATHIFA_5264

/**
 * @author qmqz
 */

@Initializable
class NathifaDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hi. Are you one of the returned townspeople?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(FRIENDLY, "Why, yes. You must be the brave adventurer that Maisa told us was seeking our return.").also { stage++ }
            1 -> player(FRIENDLY, "I guess so.").also { stage++ }
            2 -> options("I would like to buy some of your produce.", "Why were you stuck in Menaphos?", "It was nice to meet you.").also { stage++ }
            3 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> npcl(FRIENDLY, "Well, it all started the day that the plagues broke out here.").also { stage = 10 }
                3 -> end()
            }

            10 -> npcl(FRIENDLY, "I had gone into Menaphos in the morning to get some flour and chocolate powder and, when it came time for me to return, the gates were shut.").also { stage++ }
            11 -> player(ASKING, "So the guards wouldn't let you back?").also { stage++ }
            12 -> npcl(THINKING, "No, not at first. I actually didn't want to return. I didn't want to come down with the plagues so I stayed.").also { stage++ }
            13 -> player(ASKING, "What happened then?").also { stage++ }
            14 -> npcl(NEUTRAL, "Well, word must have traveled back to the slaves that their fellow slaves in Sophanem no longer had to work and so they rioted.").also { stage++ }
            15 -> player(WORRIED, "That must have been scary.").also { stage++ }
            16 -> npcl(FRIENDLY, "It was, but Coenus, chief gate keeper of the city, stamped the revolt down fairly fast...and quite brutally.").also { stage++ }
            17 -> player(ASKING, "If the revolt was stamped out, why are the gates not open?").also { stage++ }
            18 -> npcl(WORRIED, "No, it was only really stopped in the Imperial and Merchant districts of the city. There is still trouble brewing in the Worker and Ports districts.").also { stage++ }
            19 -> npcl(ASKING, "Now, if you wouldn't mind, I'd rather not dwell any further on the past. Can I interest you in my goods?").also { stage++ }
            20 -> options("Yes, please.", "No, thanks.").also { stage++ }
            21 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> end()
            }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NATHIFA_5264)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return NathifaDialogue(player)
    }
}