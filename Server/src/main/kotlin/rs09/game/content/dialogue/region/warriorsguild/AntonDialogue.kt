package rs09.game.content.dialogue.region.warriorsguild

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ANTON_4295

/**
 * @author qmqz
 */

@Initializable
class AntonDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(ASKING, "Ahhh, hello there. How can I help?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> playerl(FRIENDLY, "Looks like you have a good selection of weapons around here...").also { stage++ }
            1 -> npcl(FRIENDLY, "Indeed so, specially imported from the finest smiths around the lands, take a look at my wares.").also { stage++ }
            2 -> npc.openShop(player)
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ANTON_4295)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AntonDialogue(player)
    }
}