package rs09.game.content.dialogue.region.canifis

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.RUFUS_1038
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class RufusDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hi!")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> npcl(FRIENDLY, "Grrreetings frrriend! Welcome to my worrrld famous food emporrrium! All my meats are so frrresh you'd swear you killed them yourrrself!").also { stage++ }

            1 -> options("Why do you only sell meats?", "Do you sell cooked food?", "Can I buy some food?").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(FRIENDLY, "Why do you only sell meats?").also { stage = 10 }
                2 -> player(FRIENDLY, "Do you sell cooked food?").also { stage = 20 }
                3 -> player(FRIENDLY, "Can I buy some food?").also { stage = 30 }
            }

            10 -> npcl(ASKING, "What? Why, what else would you want to eat? What kind of lycanthrrrope are you anyway?").also { stage++ }
            11 -> player(WORRIED, "...A vegetarian one?").also { stage++ }
            12 -> npc(SUSPICIOUS, "Vegetarrrian...?").also { stage++ }
            13 -> player(GUILTY, "Never mind.").also { stage = END_DIALOGUE }

            20 -> npcl(ASKING, "Cooked food? Who would want that? You lose all the flavourrr of the meat when you can't taste the blood!").also { stage = END_DIALOGUE }

            30 -> npcl(FRIENDLY, "Cerrrtainly!").also { stage++ }
            31 -> npc.openShop(player)

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(RUFUS_1038)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return RufusDialogue(player)
    }
}