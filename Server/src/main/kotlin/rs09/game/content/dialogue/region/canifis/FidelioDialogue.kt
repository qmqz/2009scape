package rs09.game.content.dialogue.region.canifis

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.FIDELIO_1040
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class FidelioDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        player(FRIENDLY, "Hello there.")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> npcl(SCARED, "H-hello. You l-look like a s-stranger to these p-parts. Would you l-like to buy something? I h-have some s- special offers at the m-minute...some s-sample bottles for s-storing s-snail slime.").also { stage++ }

            1 -> options("Yes, please.", "No thanks.", "Why are your prices so high?").also { stage++ }
            2 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes, please.").also { stage = 10 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 20 }
                3 -> player(FRIENDLY, "Why are your prices so high?").also { stage = 30 }
            }

            10 -> npc.openShop(player)

            20 -> npcl(ASKING, "(sigh) Th-that's okay. Nobody ever w-wants to buy my wares. Oh, s-sure, if it was food or c-clothes they would though!").also { stage = END_DIALOGUE }

            30 -> npcl(SCARED, "As you p-probably know, the h-humans hate our kind and k-keep us trapped here. To get my s-stocks I have to sneak into the human lands in s-secret!").also { stage++ }
            31 -> npcl(SCARED, "All the s-secrecy and d-danger has played h-havoc with my nerves!").also { stage++ }
            32 -> player(ANNOYED, "But all your stuff is overpriced junk!").also { stage++ }
            33 -> npcl(ANNOYED, "N-not at all! They are v-valuable human artefacts smuggled here at g-great personal risk!").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(FIDELIO_1040)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return FidelioDialogue(player)
    }
}