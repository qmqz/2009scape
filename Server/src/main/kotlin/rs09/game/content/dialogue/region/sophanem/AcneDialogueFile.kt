package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import rs09.game.content.dialogue.DialogueFile
import rs09.game.interaction.item.withnpc.AcnePotion
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

class AcneDialogueFile(private var npcVal: Int) : DialogueFile() {

    val i = AcnePotion()

    override fun handle(interfaceId: Int, buttonId: Int) {
        when (stage) {
            0 -> {
                when (npcVal) {
                    0 -> {
                        npc = NPC(i.slave1)
                        npc(ASKING, "What are you doing?").also { stage++ }
                    }
                    1 -> {
                        npc = NPC(i.slave2)
                        npc(ASKING, "What are you doing?").also { stage++ }
                    }
                    2 -> {
                        npc = NPC(i.highPriest)
                        npcl(ANNOYED, "Hey, hey, hey! What in the name of sweet suffering Icthlarin are you doing?").also { stage = 11 }
                    }
                    3 -> {
                        npc = NPC(i.emablmer)
                        npc(ASKING, "What are you doing?").also { stage = 15 }
                    }
                    4 -> {
                        npc = NPC(i.carpenter)
                        npc(ASKING, "What are you doing?").also { stage = 20 }
                    }
                    5 -> {
                        npc = NPC(i.raetul)
                        npc(ASKING, "What are you doing?").also { stage = 20 }
                    }
                    6 -> {
                        npc = NPC(i.siamun)
                        npc(ASKING, "What are you doing?").also { stage = 35 }
                    }
                }
            }

            1 -> playerl(THINKING, "I have this acne potion that I thought might clear up your spots?").also { stage++ }
            2 -> npc(ASKING, "What, so I can get back to work?").also { stage++ }
            3 -> player(WORRIED, "Um, no, so that you wouldn't suffer needlessly.").also { stage++ }
            4 -> npcl(ANNOYED, "I know your type, trying to ruin the small man's fun. Well, I won't have it.").also { stage++ }
            5 -> playerl(GUILTY, "Look, I'm sorry. I only had good intentions; I only wanted to help you.").also { stage = END_DIALOGUE }

            11 -> playerl(THINKING, "I have this acne potion which I thought might help ease your itching.").also { stage++ }
            12 -> npcl(ANNOYED, "All that you'll achieve by pouring this potion on me is to ruin my lovely robes.").also { stage++ }
            13 -> player(GUILTY, "Sorry, I was just trying to help.").also { stage++ }
            14 -> npcl(NEUTRAL, "The plague of spots is a divine burden placed on us, which no amount of alchemy or potion-mixing will ease.").also { stage = END_DIALOGUE }

            15 -> playerl(FRIENDLY, "I have this acne potion which I thought might help ease your itching.").also { stage++ }
            16 -> npcl(NEUTRAL, "If I thought that these spots could be cured by some potion, I would have mixed up one myself before now.").also { stage++ }
            17 -> player(GUILTY, "Sorry, I was just trying to help.").also { stage = END_DIALOGUE }

            20 -> playerl(FRIENDLY, "I have this acne potion which I thought might help ease your itching.").also { stage++ }
            21 -> npc(FRIENDLY, "Okay, let's give it a go so.").also { stage++ }
            22 -> npcl(FURIOUS, "Aghhh!!! I got some in my eye. Owwww, it stings.").also { stage++ }
            23 -> player(NEUTRAL, "Oh, stop being so soft.").also { stage++ }
            24 -> npc(ASKING, "Well?").also { stage++ }
            25 -> player(ASKING, "Well what?").also { stage++ }
            26 -> npc(ASKING, "Did it work?").also { stage++ }
            27 -> player(GUILTY, "Well...ummm...no.").also { stage++ }
            28 -> npcl(ANNOYED, "What? You intolerable, insufferable idiot. I've just burned my eye out for no reason? I would say get out of my sight but that's not far enough away.").also { stage++ }
            29 -> player(GUILTY, "Sorry, I was just trying to help.").also { stage++ }
            30 -> npc(ANNOYED, "Scram or I'll give you something to feel sorry about.").also { stage = END_DIALOGUE }

            35 -> playerl(FRIENDLY, "I have this acne potion which I thought might help ease your itching.").also { stage++ }
            36 -> npcl(ANNOYED, "Keh! Get away from me. The only potion or solution I'll have anything to with are dyes.").also { stage++ }
            37 -> player(GUILTY, "Sorry, I was just trying to help.").also { stage = END_DIALOGUE }
        }
    }
}
