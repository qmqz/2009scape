package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.URBI_5266
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class UrbiDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(SUSPICIOUS, "Hello there; are you interested in any of my fine daggers? Ideal for slipping under desert robes to give your enemies a nasty surprise...")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> options("I'm looking for daggers, yes.", "I'd rather help you in return for better prices.", "No thanks.").also { stage++ }
            1 -> when (buttonId) {
                0 -> npc.openShop(player)
                1 -> player(FRIENDLY, "I'd rather help you in return for better prices.").also { stage = 10 }
                2 -> player(NEUTRAL, "No thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npcl(FRIENDLY, "I think it will take months for me to return to normal business. If you can wait that long, however, I may have tasks for you.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(URBI_5266)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return UrbiDialogue(player)
    }
}