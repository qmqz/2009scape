package rs09.game.content.dialogue.region.keldagrim.east

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BLASIDAR_THE_SCULPTOR_2141
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BlasidarDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(OLD_CALM_TALK1,"The new statue looks beautiful, don't you agree?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> player(FRIENDLY, "Oh yes, quite.").also { stage++ }
            1 -> npc(OLD_CALM_TALK1, "My finest piece of work, without a doubt.").also { stage++ }
            2 -> playerl(ASKING, "Say, I was wondering, did you do the statues down in the mines as well?").also { stage++ }
            3 -> npc(OLD_CALM_TALK1, "What, out with the trolls?").also { stage++ }
            4 -> npcl(OLD_CALM_TALK2, "If only! But no, they've been there for many thousands of years, actually. Been there before dwarven recorded history.").also { stage++ }
            5 -> playerl(GUILTY, "No no, I'm sure they weren't there before!").also { stage++ }
            6 -> npc(OLD_CALM_TALK1, "Of course they were, don't be silly!").also { stage++ }
            7 -> playerl(SUSPICIOUS, "Alright... my mistake then.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BlasidarDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(BLASIDAR_THE_SCULPTOR_2141)
    }
}