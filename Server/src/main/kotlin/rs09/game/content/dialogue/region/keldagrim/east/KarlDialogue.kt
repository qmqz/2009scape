package rs09.game.content.dialogue.region.keldagrim.east

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.FRIENDLY
import core.game.content.dialogue.FacialExpression.OLD_DEFAULT
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.KARL_2195
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class KarlDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(OLD_DEFAULT, "Hello there, human! Come in, come in!")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> player(FRIENDLY, "Hi, nice to see you!").also { stage++ }
            1 -> npc(OLD_DEFAULT, "Tell me, what brings you here?").also { stage++ }
            2 -> playerl(FRIENDLY, "Well, first I came to this city just for the adventure and excitement, but then I knocked over this statue, you see. Well, I didn't really knock it over myself, I was on this boat.").also { stage++ }
            3 -> npc(OLD_DEFAULT, "A boat! Fascinating, go on!").also { stage++ }
            4 -> playerl(FRIENDLY, "Yes, well, this boatman was a bit rubbish, or there was something wrong with the ship, I don't know. And then I got arrested by the Black Guard.").also { stage++ }
            5 -> npc(OLD_DEFAULT, "Not the Black Guard!").also { stage++ }
            6 -> playerl(FRIENDLY, "Yes, and they took me to this Veldaban person, who pretended to be really angry but actually he was really nice and he asked me for my help. And after a whole lot of running around, I finally managed to get the statue to be rebuilt.").also { stage++ }
            7 -> npcl(OLD_DEFAULT, "Such an interesting tale! Now how did you come to be in Keldagrim again?").also { stage++ }
            8 -> player(FRIENDLY, "Never mind.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(KARL_2195)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return KarlDialogue(player)
    }
}