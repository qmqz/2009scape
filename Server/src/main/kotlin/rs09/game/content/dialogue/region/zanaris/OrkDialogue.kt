package rs09.game.content.dialogue.region.zanaris

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.content.dialogue.FacialExpression.OLD_ANGRY1
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ORK_4458
import org.rs09.consts.NPCs.ORK_4459
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class OrkDialogue(player: Player? = null) : DialoguePlugin(player){
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        pl(FacialExpression.ASKING, "Uh, you guys aren't fairies - what are you doing here?")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> nl(OLD_ANGRY1, "Grunt! Smelly, stinky human! Urgh! Leave us alone before we hurt you.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return OrkDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(ORK_4458, ORK_4459)
    }
}
