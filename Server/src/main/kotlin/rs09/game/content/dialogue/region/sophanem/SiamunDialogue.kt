package rs09.game.content.dialogue.region.sophanem

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.SIAMUN_1983
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class SiamunDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npc(ANNOYED, "Clear off you despicable vagabond.").also { stage = END_DIALOGUE }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(SIAMUN_1983)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return SiamunDialogue(player)
    }
}