package rs09.game.content.dialogue.region.ardougne

import api.isQuestComplete
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.FARMER_BRUMTY_291
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class FarmerBrumtyDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        if(isQuestComplete(player, "Sheep Herder")) {
            player(GUILTY, "Hello there. Sorry about your sheep...").also { stage = 10 }
        } else {
            player(FRIENDLY, "Hello there.").also { stage = 20 }
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            10 -> npcl(SAD, "That's okay, it had to be done for the sake of the town.").also { stage = END_DIALOGUE }

            20 -> npcl(ANNOYED, "I have all the bad luck. My sheep all run off somewhere, and then those mourners tell me they're infected!").also { stage++ }
            21 -> player(ASKING, "Well, I hope things start to look up for you.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(FARMER_BRUMTY_291)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return FarmerBrumtyDialogue(player)
    }
}