package rs09.game.content.dialogue.region.banditcamp

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.DARREN_1781
import org.rs09.consts.NPCs.EDMOND_1785
import org.rs09.consts.NPCs.EDWARD_1782
import org.rs09.consts.NPCs.IAN_1779
import org.rs09.consts.NPCs.LARRY_1780
import org.rs09.consts.NPCs.NEIL_1784
import org.rs09.consts.NPCs.RICHARD_1783
import org.rs09.consts.NPCs.SAM_1787
import org.rs09.consts.NPCs.SIMON_1786
import org.rs09.consts.NPCs.WILLIAM_1778
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class WildnernessCapeDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(FRIENDLY, "Hello there, are you interested in buying one of my special capes?")

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> options("What do team capes do?", "Yes please!", "No thanks.").also { stage++ }
            1 -> when (buttonId) {
                1 -> player(ASKING, "What do team capes do?").also { stage = 10 }
                2 -> npc.openShop(player)
                3 -> player(FRIENDLY, "No thanks.").also { stage = END_DIALOGUE }
            }

            10 -> npcl(FRIENDLY, "If you and your friends all wear the same team cape, you'll be less likely to attack your friends by accident, and you'll find it easier to attack everyone else.").also { stage++ }
            11 -> npcl(FRIENDLY, "They're very useful in the Wilderness and other player-vs-player combat areas where you might come across friends you don't want to harm.").also { stage++ }
            12 -> npc(ASKING, "So would you like to buy one?").also { stage++ }

            13 -> options("Yes please!", "No thanks.").also { stage++ }
            14 -> when (buttonId) {
                1 -> npc.openShop(player)
                2 -> player(NEUTRAL, "No thanks.").also { stage = END_DIALOGUE }
            }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(WILLIAM_1778, IAN_1779, LARRY_1780, DARREN_1781, EDWARD_1782, RICHARD_1783, NEIL_1784, EDMOND_1785, SIMON_1786, SAM_1787)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return WildnernessCapeDialogue(player)
    }
}