package rs09.game.content.dialogue.region.yanille

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ALECK_5110
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AleckDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY, "Hello.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(FRIENDLY, "Hello, hello, and a most warm welcome to my Hunter Emporium. We have everything the discerning Hunter could need.").also { stage++ }
            1 -> npcl(FRIENDLY, "Would you like me to show you our range of equipment? Or was there something specific you were after?").also { stage++ }

            2 -> options("Ok, let's see what you've got!", "I'm not interested, thanks.", "Who's that guy over there?").also { stage++ }
            3 -> when (buttonId) {
                1 -> player(FRIENDLY, "Ok, let's see what you've got!").also { stage = 10 }
                2 -> player(FRIENDLY, "I'm not interested, thanks.").also { stage = 15 }
                3 -> player(ASKING, "Who's that guy over there?").also { stage = 20 }
            }

            10 -> npc.openShop(player)

            15 -> npcl(WORRIED, "Well, if you do ever find yourself in need of the finest Hunter equipment available, then you know where to come.").also { stage = END_DIALOGUE }

            20 -> npcl(THINKING, "Him? I think he might be crazy. Either that or he's seeking attention.").also { stage++ }
            21 -> npcl(NEUTRAL, "He keeps trying to sell me these barmy looking weapons he's invented. I can't see them working, personally.").also { stage = END_DIALOGUE }

        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ALECK_5110)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AleckDialogue(player)
    }
}