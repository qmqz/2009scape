package rs09.game.content.dialogue.region.sophanem

import api.addItemOrDrop
import api.isEquipped
import api.sendItemDialogue
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.Items
import org.rs09.consts.Items.UGTHANKI_DUNG_4601
import org.rs09.consts.NPCs.NEFERTI_THE_CAMEL_2815
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class NefertitiTheCamelDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        if (isEquipped(player, Items.CAMULET_6707)) {
            options("Ask the camel about its dung.", "Say something unpleasant.", "Neither - I'm a polite person.").also { stage = 0 }
        } else {
            options("Ask the camel about its dung.", "Say something unpleasant.", "Neither - I'm a polite person.").also { stage = 100 }
        }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {

            0 -> when (buttonId) {
                1 -> playerl(ASKING, "I'm sorry to bother you, but could you spare me a little dung?").also { stage = 10 }
                2 -> {
                    when ((1..3).random()) {
                        1 -> playerl(WORRIED, "If I go near that camel, it'll probably bite my hand off.").also { stage = 30 }
                        2 -> playerl(THINKING, "Mmm... looks like that camel would make a nice kebab.").also { stage = 30 }
                        3 -> player(HALF_THINKING, "I wonder if that camel has fleas...").also { stage = 30 }
                    }
                }
                3 -> end()
            }

            10 -> npc(CHILD_FRIENDLY, "Are you serious?").also { stage++ }
            11 -> player(FRIENDLY, "Oh yes. If you'd be so kind...").also { stage++ }
            12 -> npcl(CHILD_FRIENDLY, "Well, just you close your eyes first. I'm not doing it while you're watching me!").also { stage++ }
            13 -> sendItemDialogue(player, UGTHANKI_DUNG_4601, "You receive some dung.").also { stage++ }
            14 -> {
                //not authentic, should spawn some on the ground and then you need to scoop it with a bucket
                addItemOrDrop(player, UGTHANKI_DUNG_4601, 1)
                npc(CHILD_FRIENDLY, "I hope that's what you wanted!").also { stage++ }
            }
            15 -> player(FRIENDLY, "Ohhh yes. Lovely.").also { stage = END_DIALOGUE }

            30 -> npc(CHILD_FRIENDLY, "What was that?").also { stage++ }
            31 -> player(FRIENDLY, "Er, nothing important.").also { stage++ }
            32 -> npcl(CHILD_FRIENDLY, "Wait, you're a human, aren't you? Have you come from outside this town?").also { stage++ }
            33 -> player(FRIENDLY, "Yes, I have.").also { stage++ }
            34 -> npcl(CHILD_FRIENDLY, "Oh, how wonderful! Does that mean I can finally leave this place and see the world outside?").also { stage++ }
            35 -> player(ASKING, "Wouldn't your master have to take you?").also { stage++ }
            36 -> npcl(CHILD_FRIENDLY, "My master has no interest in leaving. It's so boring here, and I'd like to travel.").also { stage++ }
            37 -> npcl(CHILD_FRIENDLY, "Travelling's what camels do best, you know.").also { stage++ }
            38 -> npcl(CHILD_FRIENDLY, "I don't suppose you could take me with you, could you?").also { stage++ }
            39 -> playerl(FRIENDLY, "I don't really have space for a camel, sorry.").also { stage++ }
            40 -> npcl(CHILD_FRIENDLY, "Oh well. Come back some time, and bring me news of the rest of the world.").also { stage = END_DIALOGUE }

            100 -> when (buttonId) {
                1 -> playerl(ASKING, "I'm sorry to bother you, but could you spare me a little dung?").also { stage = 110 }
                2 -> when ((1..3).random()) {
                    1 -> playerl(WORRIED, "If I go near that camel, it'll probably bite my hand off.").also { stage = END_DIALOGUE }
                    2 -> playerl(THINKING, "Mmm... looks like that camel would make a nice kebab.").also { stage = END_DIALOGUE }
                    3 -> player(HALF_THINKING, "I wonder if that camel has fleas...").also { stage = END_DIALOGUE }
                }
                3 -> end()
            }

            110 -> sendDialogue("The camel didn't seem to appreciate that question.").also { stage = END_DIALOGUE }


        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NEFERTI_THE_CAMEL_2815)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return NefertitiTheCamelDialogue(player)
    }
}