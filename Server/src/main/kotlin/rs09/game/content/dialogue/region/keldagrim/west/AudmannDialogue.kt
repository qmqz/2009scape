package rs09.game.content.dialogue.region.keldagrim.west

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.AUDMANN_2201
import org.rs09.consts.NPCs.AUDMANN_2202
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class AudmannDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        npc(OLD_DEFAULT,"Oh, don't bother me human.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> player(ASKING, "Why not? What's wrong?").also { stage++ }
            1 -> npcl(OLD_DEFAULT, "You are wrong, human. Your attire is outrageous. Your presence is obnoxious.").also { stage++ }
            2 -> player(THINKING, "What? What are you saying?").also { stage++ }
            3 -> npc(OLD_DEFAULT, "I'm saying you're in my way.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return AudmannDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(AUDMANN_2201, AUDMANN_2202)
    }
}