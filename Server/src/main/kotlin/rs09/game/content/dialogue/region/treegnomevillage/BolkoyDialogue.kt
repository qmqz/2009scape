package rs09.game.content.dialogue.region.treegnomevillage

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.BOLKOY_471
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class BolkoyDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        player(FRIENDLY, "Hello there.")
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npc(OLD_CALM_TALK1, "Hello stranger, new to these parts?").also { stage++ }
            1 -> npcl(OLD_CALM_TALK2, "I'm Bolkoy by the way. I'm the village shopkeeper. Would you like to buy something?").also { stage++ }
            2 -> options("What have you got?", "No thank you.").also { stage++ }
            3 -> when (buttonId) {
                1 -> player(ASKING, "What have you got?").also { stage = 10 }
                2 -> player(NEUTRAL, "No thank you.").also { stage = 20 }
            }

            10 -> npc(OLD_CALM_TALK1, "Take a look.").also { stage++ }
            11 -> npc.openShop(player)

            20 -> npc(OLD_CALM_TALK2, "Ok, maybe later.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(BOLKOY_471)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return BolkoyDialogue(player)
    }
}