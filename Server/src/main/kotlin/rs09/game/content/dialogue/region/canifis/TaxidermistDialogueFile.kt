package rs09.game.content.dialogue.region.canifis

import api.addItem
import api.amountInInventory
import api.removeItem
import api.sendDialogue
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.item.Item
import org.rs09.consts.Items
import rs09.game.content.dialogue.DialogueFile
import rs09.game.interaction.item.withnpc.TaxidermistListener
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

class TaxidermistDialogueFile(private var npcVal: Int) : DialogueFile() {

    val i = TaxidermistListener()

    override fun handle(interfaceId: Int, buttonId: Int) {
        npc = NPC(i.taxidermist)
        when (stage) {
            0 -> {
                when (npcVal) {
                    0 -> npcl(FRIENDLY, "That's a mighty fine sea bass you've caught there. I can preserve that for you for 1000 coins.").also { stage = 10 }
                    1 -> npcl(FRIENDLY, "Don't point that thing at me! I can preserve that for you for 2500 coins.").also { stage = 20 }
                    2 -> npcl(FRIENDLY, "That's quite a fearsome shark! You've done everyone a service by removing it from the sea! I can preserve that for you for 5000 coins.").also { stage = 30 }
                    3 -> npcl(FRIENDLY, "That's a very fine crawling hand. I can preserve that for you for 1000 coins.").also { stage = 40 }
                    4 -> npcl(FRIENDLY, "A cockatrice! Beautiful, isn't it? Look at the plumage! I can preserve that for you for 2000 coins.").also { stage = 50 }
                    5 -> npcl(FRIENDLY, "My, he's a scary-looking fellow, isn't he? He'll look good on your wall! I can preserve that for you for 4000 coins.").also { stage = 60 }
                    6 -> npcl(FRIENDLY, "A kurask? Splendid! Look at those horns! I can preserve that for you for 6000 coins.").also { stage = 70 }
                    7 -> npcl(FRIENDLY, "Goodness, an abyssal demon! See how it's still glowing? I'll have to use some magic to preserve that. I can preserve that for you for 12000 coins.").also { stage = 80 }
                    8 -> npcl(FRIENDLY, "This must be a King Black Dragon! I'll have to get out my heavy duty tools -- this skin's as tough as iron! I can preserve that for you for 50000 coins.").also { stage = 90 }
                    9 -> npcl(FRIENDLY, "That must be the biggest kalphite I've ever seen! Preserving insects is always tricky. I'll have to be careful... I can preserve that for you for 50000 coins.").also { stage = 100 }
                    10 -> npcl(ASKING, "That's a pretty ordinary fish, isn't it? Not really worth preserving.").also { stage = END_DIALOGUE }
                    11 -> npcl(LAUGH, "Killing a chicken is hardly worth boasting about!").also { stage = END_DIALOGUE }
                    12 -> npcl(LAUGH, "Don't be silly, I can't preserve that!").also { stage = END_DIALOGUE }
                }
            }

            10 -> options("Yes please", "No thanks").also { stage++ }
            11 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 15 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            15 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 1000) {
                    removeItem(player!!, Item(Items.COINS_995,1000))
                    removeItem(player!!, i.bigbass)
                    addItem(player!!, Items.BIG_BASS_7990)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            20 -> options("Yes please", "No thanks").also { stage++ }
            21 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 25 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            25 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 2500) {
                    removeItem(player!!, Item(Items.COINS_995,2500))
                    removeItem(player!!, i.bigswordfish)
                    addItem(player!!, Items.BIG_SWORDFISH_7992)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            30 -> options("Yes please", "No thanks").also { stage++ }
            31 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 35 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            35 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 5000) {
                    removeItem(player!!, Item(Items.COINS_995,5000))
                    removeItem(player!!, i.bigshark)
                    addItem(player!!, Items.BIG_SHARK_7994)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            40 -> options("Yes please", "No thanks").also { stage++ }
            41 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 45 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            45 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 1000) {
                    removeItem(player!!, Item(Items.COINS_995,1000))
                    removeItem(player!!, i.crawlinghand)
                    addItem(player!!, Items.CRAWLING_HAND_7982)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            50 -> options("Yes please", "No thanks").also { stage++ }
            51 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 55 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            55 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 2000) {
                    removeItem(player!!, Item(Items.COINS_995,2000))
                    removeItem(player!!, i.cockatricehead)
                    addItem(player!!, Items.COCKATRICE_HEAD_7983)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            60 -> options("Yes please", "No thanks").also { stage++ }
            61 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 65 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            65 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 4000) {
                    removeItem(player!!, Item(Items.COINS_995,4000))
                    removeItem(player!!, i.basiliskhead)
                    addItem(player!!, Items.BASILISK_HEAD_7984)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            70 -> options("Yes please", "No thanks").also { stage++ }
            71 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 75 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            75 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 6000) {
                    removeItem(player!!, Item(Items.COINS_995,6000))
                    removeItem(player!!, i.kuraskhead)
                    addItem(player!!, Items.KURASK_HEAD_7985)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            80 -> options("Yes please", "No thanks").also { stage++ }
            81 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 85 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            85 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 12000) {
                    removeItem(player!!, Item(Items.COINS_995,12000))
                    removeItem(player!!, i.abyssalhead)
                    addItem(player!!, Items.ABYSSAL_HEAD_7986)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            90 -> options("Yes please", "No thanks").also { stage++ }
            91 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 95 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            95 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 50000) {
                    removeItem(player!!, Item(Items.COINS_995,50000))
                    removeItem(player!!, i.kingblackdragonhead)
                    addItem(player!!, Items.KBD_HEADS_7987)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            100 -> options("Yes please", "No thanks").also { stage++ }
            101 -> when (buttonId) {
                1 -> player(FRIENDLY, "Yes please.").also { stage = 105 }
                2 -> player(FRIENDLY, "No thanks.").also { stage = 1000 }
            }

            105 -> {
                if (player?.let { amountInInventory(it, Items.COINS_995) }!! >= 50000) {
                    removeItem(player!!, Item(Items.COINS_995,50000))
                    removeItem(player!!, i.kalphitequeenhead)
                    addItem(player!!, Items.KQ_HEAD_7988)
                    sendDialogue(player!!, "Takes the payment and the remains, and gives you the stuffed one").also { stage = 999 }
                } else {
                    sendDialogue(player!!, "You don't have enough coins to do that.").also { stage = END_DIALOGUE }
                }
            }

            999 -> npc(FRIENDLY, "There you go!").also { stage = END_DIALOGUE }
            1000 -> npc(FRIENDLY, "All right, come back if you change your mind, eh?").also { stage = END_DIALOGUE }
        }
    }
}
