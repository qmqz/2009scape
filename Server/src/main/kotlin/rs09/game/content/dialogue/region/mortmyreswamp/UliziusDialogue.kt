package rs09.game.content.dialogue.region.mortmyreswamp

import api.questStage
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression.*
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs.ULIZIUS_1054
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class UliziusDialogue(player: Player? = null) : DialoguePlugin(player) {
    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        when (questStage(player, "Nature Spirit")) {
            0 -> player(FRIENDLY, "Hello there.").also { stage = 0 }
            100 -> player(FRIENDLY, "Hello there.").also { stage = 200 }
            else -> player(FRIENDLY, "Hello there.").also { stage = 100 }
        }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage) {
            0 -> npcl(SCARED, "What... Oh, don't creep up on me like that... I thought you were a Ghast!").also { stage++ }
            1 -> player(ASKING, "Can I go through the gate please?").also { stage++ }
            2 -> npcl(ANNOYED, "Absolutely not! I've been given strict instructions not to let anyone through. It's just too dangerous. No one gets in without Drezels say so!").also { stage++ }
            3 -> player(ASKING, "Where is Drezel?").also { stage++ }
            4 -> npcl(FRIENDLY, "Oh, he's in the temple, just go back over the bridge, down the ladder and along the hallway, you can't miss him.").also { stage = END_DIALOGUE }

            100 -> npcl(SCARED, "What... Oh, don't creep up on me like that... I thought you were a Ghast!").also { stage++ }
            101 -> player(ASKING, "Can I go through the gate please?").also { stage++ }
            102 -> npcl(ANNOYED, "Absolutely not! I've been given strict instructions not to let anyone through. It's just too dangerous. No one gets in without Drezels say so!").also { stage++ }
            103 -> player(ASKING, "But I'm doing a quest for Drezel!").also { stage++ }
            104 -> npc(FRIENDLY, "Ok, on your way then!").also { stage = END_DIALOGUE }

            200 -> npcl(SCARED, "What... Oh, don't creep up on me like that... I thought you were a Ghast!").also { stage++ }
            201 -> player(ASKING, "Can I go through the gate please?").also { stage++ }
            202 -> npcl(FRIENDLY, "Yes of course my friend, you seem to be able to handle yourself with those ghasts really well.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(ULIZIUS_1054)
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return UliziusDialogue(player)
    }
}