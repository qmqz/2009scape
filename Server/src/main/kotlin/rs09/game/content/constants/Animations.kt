package rs09.game.content.constants

class Animations {
    val MIX_POTION = 363
    val PESTLE_AND_MORTAR = 364
    val OPEN_CHEST = 536
    val PLACE_ON_TABLE = 537
    val TAKE_FROM_CHEST = 538
    val CLOSE_CHEST = 539
    val CATTLE_PROD = 799
    val EAT = 829
    val DIG_SPADE = 830
    val DIG_SPADE_LOOP = 831
    val CRAWLING = 844
    val FILL_BUCKET_WITH_SAND = 895
    val CUT_CHOCOLATE_BAR = 1989
    val EMPTY_VIAL = 2259
    val RAISE_BEER_GLASS = 2343
    val PICK_UP = 2697
    val CHURN_BUTTER = 2793
    val HOLY_WRENCH_1 = 2810
    val HOLY_WRENCH_2 = 2811
    val POTION_ON_HAND = 3283
    val DWARVEN_ROCK_CAKE_TOO_HOT_DROP = 3399
    val CRAFT_WITH_NEEDLE = 5243
    val CRAFT_WITH_KNIFE = 5244
    val BRING_HANDS_TOGETHER = 709
    val CRAFT_GENERIC = 1280
}