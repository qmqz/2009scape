package rs09.game.interaction.item.withitem.skills.crafting

import api.addItem
import api.removeItem
import api.sendMessage
import core.game.node.entity.skill.Skills
import core.game.node.entity.skill.Skills.CRAFTING
import org.rs09.consts.Items.BALL_OF_WOOL_1759
import org.rs09.consts.Items.RABBIT_FOOT_10134
import org.rs09.consts.Items.STRUNG_RABBIT_FOOT_10132
import rs09.game.interaction.InteractionListener

/**
 * @author qmqz
 */

class StrungRabbitFootListener : InteractionListener {
    override fun defineListeners() {
        onUseWith(ITEM, RABBIT_FOOT_10134, BALL_OF_WOOL_1759) { player, used, with ->
            if (player.skills.getStaticLevel(CRAFTING) >= 37) {
                if(removeItem(player, used.asItem()) && removeItem(player, with.asItem())) {
                    addItem(player, STRUNG_RABBIT_FOOT_10132, 1)
                    player.skills.addExperience(CRAFTING, 4.0)
                    sendMessage(player, "You use the wool with the rabbit foot to make a lucky necklace.")
                }
                return@onUseWith true
            }
            sendMessage(player, "Your Crafting level is not high enough to do this.")
            return@onUseWith false
        }

        onEquip(STRUNG_RABBIT_FOOT_10132){ player, _ ->
            if(player.skills.getStaticLevel(Skills.HUNTER) >= 24) {
                return@onEquip true
            }
            sendMessage(player, "Your Hunting level is not high enough to equip this.")
            return@onEquip false
        }
    }
}