package rs09.game.interaction.item.withitem.skills.crafting.questitems

import api.addItem
import api.removeItem
import api.sendMessage
import core.game.node.entity.skill.Skills
import org.rs09.consts.Items.BALL_OF_WOOL_1759
import org.rs09.consts.Items.MSPEAK_AMULET_4021
import org.rs09.consts.Items.MSPEAK_AMULET_4022
import rs09.game.interaction.InteractionListener

/**
 * @author qmqz
 */

class MSpeakAmuletListener : InteractionListener {
    override fun defineListeners() {
        onUseWith(ITEM, MSPEAK_AMULET_4022, BALL_OF_WOOL_1759) { player, used, with ->
            if(removeItem(player, used.asItem()) && removeItem(player, with.asItem())) {
                addItem(player, MSPEAK_AMULET_4021, 1)
                player.skills.addExperience(Skills.CRAFTING, 4.0)
                sendMessage(player, "You put some string on your amulet. It makes a slight 'Ook' sound.")
            }
            return@onUseWith true
        }
    }
}