package rs09.game.interaction.item.withnpc

import api.openDialogue
import org.rs09.consts.Items.ABYSSAL_HEAD_7979
import org.rs09.consts.Items.BASILISK_HEAD_7977
import org.rs09.consts.Items.BIG_BASS_7989
import org.rs09.consts.Items.BIG_SHARK_7993
import org.rs09.consts.Items.BIG_SWORDFISH_7991
import org.rs09.consts.Items.COCKATRICE_HEAD_7976
import org.rs09.consts.Items.CRAWLING_HAND_4133
import org.rs09.consts.Items.KBD_HEADS_7980
import org.rs09.consts.Items.KQ_HEAD_7981
import org.rs09.consts.Items.KURASK_HEAD_7978
import org.rs09.consts.Items.RAW_ANCHOVIES_321
import org.rs09.consts.Items.RAW_BASS_363
import org.rs09.consts.Items.RAW_CHICKEN_2138
import org.rs09.consts.Items.RAW_CHICKEN_4289
import org.rs09.consts.Items.RAW_COD_341
import org.rs09.consts.Items.RAW_HERRING_345
import org.rs09.consts.Items.RAW_LOBSTER_377
import org.rs09.consts.Items.RAW_MACKEREL_353
import org.rs09.consts.Items.RAW_MANTA_RAY_389
import org.rs09.consts.Items.RAW_PIKE_349
import org.rs09.consts.Items.RAW_SALMON_331
import org.rs09.consts.Items.RAW_SARDINE_327
import org.rs09.consts.Items.RAW_SEA_TURTLE_395
import org.rs09.consts.Items.RAW_SHARK_383
import org.rs09.consts.Items.RAW_SHRIMPS_317
import org.rs09.consts.Items.RAW_SWORDFISH_371
import org.rs09.consts.Items.RAW_TROUT_335
import org.rs09.consts.Items.RAW_TUNA_359
import org.rs09.consts.NPCs
import rs09.game.content.dialogue.region.canifis.TaxidermistDialogueFile
import rs09.game.interaction.InteractionListener

open class TaxidermistListener() : InteractionListener {

    val taxidermist = NPCs.TAXIDERMIST_4246

    val bigbass = BIG_BASS_7989
    val bigswordfish = BIG_SWORDFISH_7991
    val bigshark = BIG_SHARK_7993
    val crawlinghand = CRAWLING_HAND_4133
    val cockatricehead = COCKATRICE_HEAD_7976
    val basiliskhead = BASILISK_HEAD_7977
    val kuraskhead = KURASK_HEAD_7978
    val abyssalhead = ABYSSAL_HEAD_7979
    val kingblackdragonhead = KBD_HEADS_7980
    val kalphitequeenhead = KQ_HEAD_7981

    val fishies = intArrayOf(RAW_SHRIMPS_317, RAW_ANCHOVIES_321, RAW_SARDINE_327, RAW_SALMON_331, RAW_TROUT_335, RAW_COD_341,
        RAW_HERRING_345, RAW_PIKE_349, RAW_MACKEREL_353, RAW_TUNA_359, RAW_BASS_363, RAW_SWORDFISH_371, RAW_LOBSTER_377, RAW_SHARK_383,
        RAW_MANTA_RAY_389, RAW_SEA_TURTLE_395)

    val chicken = intArrayOf(RAW_CHICKEN_2138, RAW_CHICKEN_4289)

    override fun defineListeners() {
        onUseWith(NPC, bigbass, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(0))
            return@onUseWith false
        }
        onUseWith(NPC, bigswordfish, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(1))
            return@onUseWith false
        }
        onUseWith(NPC, bigshark, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(2))
            return@onUseWith false
        }
        onUseWith(NPC, crawlinghand, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(3))
            return@onUseWith false
        }
        onUseWith(NPC, cockatricehead, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(4))
            return@onUseWith false
        }
        onUseWith(NPC, basiliskhead, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(5))
            return@onUseWith false
        }
        onUseWith(NPC, kuraskhead, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(6))
            return@onUseWith false
        }
        onUseWith(NPC, abyssalhead, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(7))
            return@onUseWith false
        }
        onUseWith(NPC, kingblackdragonhead, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(8))
            return@onUseWith false
        }
        onUseWith(NPC, kalphitequeenhead, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(9))
            return@onUseWith false
        }

        onUseWith(NPC, fishies, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(10))
            return@onUseWith false
        }

        onUseWith(NPC, chicken, taxidermist) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(11))
            return@onUseWith false
        }

        onUseWithWildcard(NPC, { _, _ -> true}) {
                player, _, _ -> openDialogue(player, TaxidermistDialogueFile(12))
            return@onUseWithWildcard false
        }


    }
}