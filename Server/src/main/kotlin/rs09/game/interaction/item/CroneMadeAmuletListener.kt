package rs09.game.interaction.item

import api.sendMessage
import org.rs09.consts.Items.CRONE_MADE_AMULET_10500
import rs09.game.interaction.IntType
import rs09.game.interaction.InteractionListener

/**
 * @author qmqz
 */

class CroneMadeAmuletListener : InteractionListener {
    override fun defineListeners() {
        on(CRONE_MADE_AMULET_10500, IntType.ITEM, "wear") { player, _ ->
            sendMessage(player, "Your ghostliness isn't ethereal enough to wear this. Perhaps you should wait a few hundred years or so?")
            return@on true
        }
    }
}