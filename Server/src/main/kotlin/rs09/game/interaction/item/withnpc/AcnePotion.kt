package rs09.game.interaction.item.withnpc

import api.openDialogue
import core.game.node.entity.npc.NPC
import org.rs09.consts.Items.POTION_195
import org.rs09.consts.NPCs.CARPENTER_5289
import org.rs09.consts.NPCs.EMBALMER_1980
import org.rs09.consts.NPCs.HIGH_PRIEST_1986
import org.rs09.consts.NPCs.RAETUL_1982
import org.rs09.consts.NPCs.SIAMUN_1983
import org.rs09.consts.NPCs.SLAVE_1978
import org.rs09.consts.NPCs.SLAVE_1979
import rs09.game.content.dialogue.region.sophanem.AcneDialogueFile
import rs09.game.interaction.InteractionListener

open class AcnePotion() : InteractionListener {
    private val potion = POTION_195

    val slave1 = SLAVE_1978
    val slave2 = SLAVE_1979
    val emablmer = EMBALMER_1980
    val raetul = RAETUL_1982
    val siamun = SIAMUN_1983
    val highPriest = HIGH_PRIEST_1986
    val carpenter = CARPENTER_5289


    override fun defineListeners() {
        onUseWith(NPC, potion, slave1) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(0))
            return@onUseWith false
        }

        onUseWith(NPC, potion, slave2) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(1))
            return@onUseWith false
        }

        onUseWith(NPC, potion, highPriest) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(2))
            return@onUseWith false
        }

        onUseWith(NPC, potion, emablmer) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(3))
            return@onUseWith false
        }

        onUseWith(NPC, potion, carpenter) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(4))
            return@onUseWith false
        }

        onUseWith(NPC, potion, raetul) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(5))
            return@onUseWith false
        }

        onUseWith(NPC, potion, siamun) {
                player, _, _ -> openDialogue(player, AcneDialogueFile(6))
            return@onUseWith false
        }

    }
}