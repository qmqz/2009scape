package rs09.game.interaction.item.withitem.skills.crafting

import api.addItem
import api.anim
import api.animate
import api.removeItem
import core.game.node.entity.skill.Skills
import core.game.system.task.Pulse
import org.rs09.consts.Items
import org.rs09.consts.Items.OIL_LAMP_4525
import org.rs09.consts.Items.OIL_LANTERN_FRAME_4540
import rs09.game.interaction.InteractionListener

/**
 * @author qmqz
 * TODO animations/messages
 */

class OilLampListener : InteractionListener {
    override fun defineListeners() {
        onUseWith(ITEM, OIL_LAMP_4525, OIL_LANTERN_FRAME_4540) { player, used, with ->
                player.pulseManager.run(object : Pulse() {
                    var counter = 0
                    override fun pulse(): Boolean {
                        when (counter++) {
                            0 -> animate(player, anim.CRAFT_GENERIC)
                            2 -> {
                                if(removeItem(player, used.asItem()) && removeItem(player, with.asItem())) {
                                    addItem(player, Items.OIL_LANTERN_4535, 1)
                                    player.skills.addExperience(Skills.CRAFTING, 50.0)
                                    //sendMessage(player, "")
                                }
                            }
                        }
                        return false
                    }
                })
            return@onUseWith true
        }
    }
}