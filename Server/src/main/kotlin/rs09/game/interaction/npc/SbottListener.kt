package rs09.game.interaction.npc

import core.game.node.entity.skill.crafting.TanningProduct
import org.rs09.consts.NPCs.SBOTT_1041
import rs09.game.interaction.InteractionListener

class SbottListener : InteractionListener {

    override fun defineListeners() {
        on(SBOTT_1041, NPC, "trade") { player, _ ->
            TanningProduct.open(player, 1041)
            return@on true
        }
    }
}