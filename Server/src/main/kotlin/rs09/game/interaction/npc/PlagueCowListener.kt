package rs09.game.interaction.npc

import api.*
import core.game.content.dialogue.FacialExpression
import org.rs09.consts.NPCs
import rs09.game.interaction.InteractionListener
import rs09.tools.END_DIALOGUE

class PlagueCowListener : InteractionListener {

    var a = intArrayOf(NPCs.PLAGUE_COW_1998, NPCs.PLAGUE_COW_1999, NPCs.PLAGUE_COW_2000)

    override fun defineListeners() {
        on(a, NPC, "pet") { player, _ ->
            when ((1..2).random()) {
                1 -> sendPlayerDialogue(player, "Awww, look at the poor sick cow. We'd never make a burger out of you.", FacialExpression.FRIENDLY).also { END_DIALOGUE }
                2 -> sendPlayerDialogue(player, "Don't worry cow, I'm sure somebody will find a cure for this plague soon.", FacialExpression.FRIENDLY).also { END_DIALOGUE }
            }
            return@on true
        }
    }
}